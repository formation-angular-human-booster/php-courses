<?php
require_once 'connexion_bdd.php';
require_once 'functions.php';
$id = $_GET['id'];
$planet = getOnePlanet($pdo, $id);

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $retourValidation = validateFormEdit();
    $errors = $retourValidation['errors'];
    $fileUrl = $retourValidation['image'];
    update($pdo, $fileUrl, $id);
    $errors = $retourValidation['errors'];
    header('Location: index.php');
}
?>
<html>
<head>
    <?php
    include 'parts/head.php';
    ?>
</head>
<body>
<form method="post" action="edit-planet.php?id=<?php echo($id);?>" enctype="multipart/form-data">
    <label>Nom de la planète</label>
    <input name="name" minlength="3" class="form-control" value="<?php echo($planet['name'])?>" placeholder="Nom de la planète">
    <label>Status de la planète</label>
    <input name="status" class="form-control" value="<?php echo($planet['status'])?>" placeholder="Status">
    <label>Terrain</label>
    <input name="terrain" class="form-control" value="<?php echo($planet['terrain'])?>" placeholder="Terrain">
    <label>Allegiance</label>
    <select name="allegiance" class="form-control" placeholder="Allegiance">
        <?php
        foreach (getAllegiances() as $allegiance) {
            $selected = '';
            if($allegiance === $planet['allegiance']){
                $selected = 'selected';
            }

            echo('<option '.$selected.' value="' . $allegiance . '">' . $allegiance . '</option>');
        }
        ?>
    </select>
    <label>Key facts</label>
    <textarea name="key_fact" class="form-control" placeholder="Key facts"><?php echo($planet['key_fact']);?>
    </textarea><br>
    <img style="max-width: 100px;" src="uploads/<?php echo($planet['image']);?>"><br>
    <label>Si cette image ne vous convient pas veuilles en uploader une nouvelle</label><br>

    <input type="file" name="image"><br><br>
    <input type="submit">
    <?php
        displayError($errors);
    ?>
</form>
</body>
</html>
