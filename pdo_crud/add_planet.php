<html>
<?php
require_once 'connexion_bdd.php';
require_once 'functions.php';

$errors = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $retourValidation = validateForm();
    $errors = $retourValidation['errors'];
    if (count($errors) == 0) {
        $image = $retourValidation['image'];
        addBdd($pdo, $image);
        header('location: index.php');
    }

}


include 'parts/head.php';

?>
<body>
<?php
//require_once 'connexion_bdd.php';


?>
<form method="post" action="add_planet.php" enctype="multipart/form-data">
    <label>Nom de la planète</label>
    <input name="name" minlength="3" class="form-control"
        <?php
        if (isset($_POST['name'])) {
            echo('value="' . $_POST['name'] . '"');
        }
        ?>
           placeholder="Nom de la planète">
    <label>Status de la planète</label>
    <input name="status" class="form-control" placeholder="Status">
    <label>Terrain</label>
    <input name="terrain" class="form-control" placeholder="Terrain">
    <label>Allegiance</label>
    <select name="allegiance" class="form-control" placeholder="Allegiance">
        <option selected></option>
        <?php
        foreach (getAllegiances() as $allegiance) {
            echo('<option value="' . $allegiance . '">' . $allegiance . '</option>');
        }
        ?>
    </select>
    <label>Key facts</label>
    <textarea name="key_fact" class="form-control" placeholder="Key facts"></textarea><br>
    <label>Image</label>
    <input type="file" name="image"><br><br>
    <input type="submit">
    <?php
        displayError($errors);
    ?>
</form>
</body>
</html>