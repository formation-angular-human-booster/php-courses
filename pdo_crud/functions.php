<?php
function getAllegiances() {
    $allegiance = [
        'Auvergne',
        'limousin',
        'test'
    ];
     return $allegiance;
 }

function addBdd($pdo, $image) {
    $req = $pdo->prepare( 'INSERT INTO planets(name, status, terrain , allegiance, key_fact, image)
    VALUES(:name, :status, :terrain, :allegiance, :key_fact, :image)');
    $req->execute([
        'name'=> $_POST['name'],
        'status'=>$_POST['status'],
        'terrain'=> $_POST['terrain'],
        'allegiance'=> $_POST['allegiance'],
        'key_fact'=> $_POST['key_fact'],
        'image'=> $image
    ]);
}

function getOnePlanet($pdo, $id){
    $req = $pdo->prepare('SELECT * FROM planets WHERE id = :id');
     $req->execute([
        'id'=> $id
    ]);

    return $req->fetch();
}

function validateForm() {
    $allowedFormat = ['image/jpeg', 'image/png'];
    $errors = [];
    $fileUrl = null;
    if(empty($_POST['name'])){
        $errors[] = 'Veuillez saisir un nom';
    }

    if(strlen($_POST['name'])<=3){
        $errors[] = 'Le champ nom doit au moins contenir 3 caractères';
    }

    if(empty($_POST['status'])){
        $errors[] = 'Veuillez saisir un status';
    }

    if(empty($_POST['terrain'])) {
        $errors[] = 'Veuillez saisir le terrain';
    }

    if(empty($_POST['allegiance'])) {
        $errors[] = 'Veuillez saisir l\'allegiance';
    }

    if(empty($_POST['key_fact'])) {
        $errors[] = 'Veuillez saisir la key fact';
    }

    if(count($errors) == 0){
        if(in_array($_FILES['image']['type'], $allowedFormat)){
            if($_FILES['image']['size']<80000){

                $extension = explode('/', $_FILES['image']['type'])[1];
                $fileUrl = uniqid().'.'.$extension;
                move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/'.$fileUrl);
            } else {
                $errors[] = 'impossible d\'ajouter un fichier ci lourd';
            }

        } else {
            $errors[] = 'impossible j\'accepte seulement les PNG';
        }
    }

    return ['errors'=> $errors, 'image'=> $fileUrl];
}

function validateFormEdit(){

    $allowedFormat = ['image/jpeg', 'image/png'];
    $errors = [];
    $fileUrl = null;
    if(empty($_POST['name'])){
        $errors[] = 'Veuillez saisir un nom';
    }

    if(strlen($_POST['name'])<=3){
        $errors[] = 'Le champ nom doit au moins contenir 3 caractères';
    }

    if(empty($_POST['status'])){
        $errors[] = 'Veuillez saisir un status';
    }

    if(empty($_POST['terrain'])) {
        $errors[] = 'Veuillez saisir le terrain';
    }

    if(empty($_POST['allegiance'])) {
        $errors[] = 'Veuillez saisir l\'allegiance';
    }

    if(empty($_POST['key_fact'])) {
        $errors[] = 'Veuillez saisir la key fact';
    }

    if(count($errors) == 0){
        if($_FILES['image']['size'] != 0){
            if(in_array($_FILES['image']['type'], $allowedFormat)){
                if($_FILES['image']['size']<80000){

                    $extension = explode('/', $_FILES['image']['type'])[1];
                    $fileUrl = uniqid().'.'.$extension;
                    move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/'.$fileUrl);
                } else {
                    $errors[] = 'impossible d\'ajouter un fichier ci lourd';
                }

            } else {
                $errors[] = 'impossible j\'accepte seulement les PNG';
            }
        }

    }

    return ['errors'=> $errors, 'image'=> $fileUrl];
}

function displayError($errors){
    if (count($errors) != 0) {
        echo(' <h2>Erreurs lors de la dernière soumission du formulaire : </h2>');
        foreach ($errors as $error) {
            echo('<div class="error">' . $error . '</div>');
        }
    }
}

function update($pdo, $fileUrl, $id){

    if(is_null($fileUrl)){
        $req = $pdo->prepare('UPDATE planets SET name = :name,
                   status = :status,
                   terrain = :terrain ,
                   allegiance = :allegiance,
                   key_fact = :key_fact WHERE id = :id');
        $req->execute([
            'name'=> $_POST['name'],
            'status'=>$_POST['status'],
            'terrain'=> $_POST['terrain'],
            'allegiance'=> $_POST['allegiance'],
            'key_fact'=> $_POST['key_fact'],
            'id'=> $id
        ]);
    } else {
        $req = $pdo->prepare('UPDATE planets SET name = :name,
                   status = :status,
                   terrain = :terrain ,
                   allegiance = :allegiance,
                   key_fact = :key_fact,
                    image = :image WHERE id = :id');
        $req->execute([
            'name'=> $_POST['name'],
            'status'=>$_POST['status'],
            'terrain'=> $_POST['terrain'],
            'allegiance'=> $_POST['allegiance'],
            'key_fact'=> $_POST['key_fact'],
            'image'=> $fileUrl,
            'id'=> $id
        ]);
    }
}