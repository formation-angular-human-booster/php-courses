<?php
    class Animal {
        public $name;
        private $age;
        public static $typeAnimal = ['Herbivore', 'Carnivore'];

        public function getName(){
            return $this->name;
        }

        public function getAge() {
            return $this->age;
        }

        public function __construct($name, $age)
        {
            echo('Appel de mon constructeur -> Je cré un animal qui s\'appel '.$name.' et qui à '.$age.' année(s) <br>');
            $this->name = $name;
            $this->age = $age;
        }

        public function __destruct()
        {
            echo('Appel de mon destructeur -> Cet animal n\'existe plus ! ');
        }

    }
?>