<?php
    require 'include.php';

    // Page par défaut de notre application
    if(!isset($_GET['controller']) || !isset($_GET['action'])){
        header('Location: index.php?controller=car&action=list');
    }

    elseif($_GET['controller'] == 'car' ){
        $controller = new CarController();
        if($_GET['action'] == 'detail' && isset($_GET['id'])){
            $controller->detailCar();
        } elseif ($_GET['action'] == 'list'){
            $controller->listCar();
        }
    }
?>