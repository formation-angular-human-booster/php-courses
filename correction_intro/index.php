<?php
    require 'class/Category.php';
    require 'class/Article.php';
    require 'class/Utilisateur.php';
    // Ici nous créons un article sans id. Il aura la valeur null
    // CF: constructeur class/Article.php
    $article = new Article("Clermont-Foot en L1", 10, 100, "test", "test");
    var_dump($article);
    // Ici nous créons un article AVEC un Id. L'id de l'article aura la valeur 1
    $articleWithId = new Article("Un deuxième article", 20, 150, "test2", "test2", 1);
    var_dump($articleWithId);

    echo($articleWithId->getId());

    $articleWithId->setName("Il fait froid à sainté");

    var_dump($articleWithId->getName());
    var_dump($articleWithId);
?>