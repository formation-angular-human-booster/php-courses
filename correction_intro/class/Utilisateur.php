<?php
    class Utilisateur{
        private $id;
        private $nom;
        private $prenom;
        private $username;
        private $motDePasse;

        /**
         * Utilisateur constructor.
         * @param $nom
         * @param $prenom
         * @param $username
         * @param $motDePasse
         */
        public function __construct($nom, $prenom, $username, $motDePasse)
        {
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->username = $username;
            $this->motDePasse = $motDePasse;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id): void
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getNom()
        {
            return $this->nom;
        }

        /**
         * @param mixed $nom
         */
        public function setNom($nom): void
        {
            $this->nom = $nom;
        }

        /**
         * @return mixed
         */
        public function getPrenom()
        {
            return $this->prenom;
        }

        /**
         * @param mixed $prenom
         */
        public function setPrenom($prenom): void
        {
            $this->prenom = $prenom;
        }

        /**
         * @return mixed
         */
        public function getUsername()
        {
            return $this->username;
        }

        /**
         * @param mixed $username
         */
        public function setUsername($username): void
        {
            $this->username = $username;
        }

        /**
         * @return mixed
         */
        public function getMotDePasse()
        {
            return $this->motDePasse;
        }

        /**
         * @param mixed $motDePasse
         */
        public function setMotDePasse($motDePasse): void
        {
            $this->motDePasse = $motDePasse;
        }
    }
?>