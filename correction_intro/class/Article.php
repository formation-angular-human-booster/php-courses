<?php
    class Article{
        // Nous avons ici un attribut static. Il appartient à la classe
        // et non à l'objet.
        private static $className = 'Article';

        // Nous avons ici un attribut privé. Notre objet pourra y accèder
        // Nous ne pourrons pas y accéder en dehors de notre objet
        private $id;

        private $name;

        private $quantite;

        private $prix;

        private $photo;

        private $category;

        // J'ai une fonction magique le constructeur. Elle est appelé quand on
        // cré un nouvel objet (quand nous utiliserons le mot clé new).
        // Elle prend en paramètre les valeurs que l'on souhaite assigner à notre
        // objet.
        public function __construct($name, $quantite, $prix, $photo, $category, $id=null){
            // Ici le passage en paramètre de mon id est optionnel
            // Si je ne le passe pas il sera égal à la valeur par défaut
            // dans notre cas null. Sinon il prendra la valeur passée
            $this->id = $id;

            // Ici nous assignons les valeurs
            // passées en paramètre aux attributs de notre objet
            $this->name = $name;

            // Ici nous assignons les valeurs
            // passées en paramètre aux attributs de notre objet
            $this->quantite = $quantite;

            // Ici nous assignons les valeurs
            // passées en paramètre aux attributs de notre objet
            $this->prix = $prix;

            // Ici nous assignons les valeurs
            // passées en paramètre aux attributs de notre objet
            $this->photo = $photo;

            // Ici nous assignons les valeurs
            // passées en paramètre aux attributs de notre objet
            $this->category = $category;
        }

        // Ici nous avons une fonction static. Elle appartient à la classe
        // et non à l'objet. Elle retourne l'attribut static className
        // grâce au mot clé self::
        public static function getClassName(){
            return self::$className;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getId(){
            return $this->id;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setId($id){
            $this->id = $id;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getName(){
            return $this->name;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setName($name){
            $this->name = $name;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getQuantite()
        {
            return $this->quantite;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setQuantite($quantite): void
        {
            $this->quantite = $quantite;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getPrix()
        {
            return $this->prix;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setPrix($prix): void
        {
            $this->prix = $prix;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getPhoto()
        {
            return $this->photo;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setPhoto($photo): void
        {
            $this->photo = $photo;
        }

        // Cette fonction appartient à l'objet, elle est publique et pourra donc
        // être utilisée en dehors de l'objet
        public function getCategory()
        {
            return $this->category;
        }

        // Fonction publique qui prend un paramètre.
        // Cette fonction ira mettre à jour un attribut de notre objet
        // en fonction du paramètre qui lui est passé.
        public function setCategory($category): void
        {
            $this->category = $category;
        }
    }
?>