<?php
    session_start();
    if($_SESSION['utilisateur']) {
        header('Location: homepage.php');
    } else {
        header('Location: login.php');
    }
?>