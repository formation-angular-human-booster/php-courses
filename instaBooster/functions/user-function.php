<?php
function isUserConnecter(){
    if($_SESSION['utilisateur']){
        return $_SESSION['utilisateur'];
    } else {
        header('Location: login.php');
    }
}
function login($pdo, $login, $password) {
    $errors = [];

    try{
        $req = $pdo->prepare(
            'SELECT * FROM utilisateur where email = :email OR pseudo = :pseudo');
        $req->execute([
            'email' => $login,
            'pseudo' => $login
        ]);
    } catch (PDOException $exception){
        var_dump($exception);
        die();
    }

    $res = $req->fetch();
    if($res == false){

        $errors[] = 'UtilisateurModel inconnu';
        session_destroy();
    } else {

        $_SESSION['utilisateur'] = $res;
    }
    return $errors;
}

function registerUser($pdo, $errors){
    try{
        $req = $pdo->prepare(
            'INSERT INTO utilisateur(login, email, prenom , nom, password)
    VALUES(:pseudo, :email, :prenom, :nom, :password)');
        $req->execute([
            'pseudo' => $_POST['pseudo'],
            'email' => $_POST['email'],
            'prenom' => $_POST['prenom'],
            'nom' => $_POST['nom'],
                'password' => md5($_POST['password'])
        ]);
    } catch (PDOException $exception){
        if($exception->getCode() === '23000'){
            $errors[] = 'Email déjà utilisé';
        }
    }
    return $errors;
}

function validateFormUser(){
    $error = [];
    if(empty($_POST['email'])){
        $error[] = 'Veuillez saisir l\'email';
    }
    if(empty($_POST['pseudo'])){
        $error[] = 'Veuillez saisir le pseudo';
    }

    if(empty($_POST['password'])){
        $error[] = 'Veuillez saisir le mot de passe';
    }

    if(empty($_POST['nom'])){
        $error[] = 'Veuillez saisir le nom';
    }

    if(empty($_POST['nom'])){
        $error[] = 'Veuillez saisir le prénom';
    }

    return $error;
}
?>