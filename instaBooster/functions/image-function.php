<?php
session_start();
    function uploadImage() {
        $errors = [];
        $fileName = '';
        $allowedExtension = ['image/jpg', 'image/png'];
        if($_FILES['photo']['size'] === 0){
            $errors[] = 'Veuillez ajouter une photo !';
        }
        if(in_array($_FILES['photo']['type'], $allowedExtension)){
            if($_FILES['photo']['size']<100000){

                // Transform image/png en png
                $fileName = uniqid().'.'.explode('/', $_FILES['photo']['type'])[1];
                move_uploaded_file($_FILES['photo']['tmp_name'], 'assets/images/upload/'.$fileName);
            } else {
                $error[] = 'Ce fichier est trop lourd';
            }

        } else {
            $error[] = 'Ce type de fichier n\'est pas pris en compte';
        }


        return ['errors'=> $errors, 'filename'=> $fileName];
    }

    function addBddImage($pdo, $fileName){
        $dateToday = new \DateTime();
        $nomUser = $_SESSION['utilisateur']['nom'].' '.$_SESSION['utilisateur']['prenom'];
        $localisation = $_POST['lieu'];

        try{
            $req = $pdo->prepare(
                'INSERT INTO photo(file_name, lieu_publi, date_publication , nom_prenom_utilisateur)
    VALUES(:filename, :lieu_publi, NOW() , :nom_prenom_utilisateur)');
            $req->execute([
                'filename' => $fileName,
                'lieu_publi' => $localisation,
                'nom_prenom_utilisateur' => $nomUser]);
        } catch (PDOException $exception){
            var_dump($exception);
            die();
        }

    }
?>