<?php
require_once 'functions/user-function.php';
require_once 'pdo_connexion.php';

if ( $_SERVER['REQUEST_METHOD'] === 'POST'){
 // je ferais le traitement de mon formulaire d'inscription

    $errors = validateFormUser();

    if(count($errors) ==  0){
        $errors = registerUser($pdo, $errors);
        if(count($errors) == 0){
            header('Location: login.php');
        }
    }

}
?>
<html>
<head>
    <?php
    include 'stylesheets.php';
    ?>
</head>
<body>
<h2>S'enregistrer</h2>

<form method="post" action="register.php">
    <input type="email" name="email" required placeholder="email">
    <input type="text" name="pseudo" required placeholder="Pseudo">
    <input type="password" name="password"  placeholder="Mot de passe">
    <input type="text" name="nom" required placeholder="Nom">
    <input type="text" name="prenom" required placeholder="Prénom">
    <input type="submit">
</form>


<ul>
<?php
 if(count($errors)>0){
     echo('<h2>Les erreurs : </h2>');
     foreach ($errors as $error){
         echo('<li>'.$error.'</li>');
     }
 }
?>
</ul>
<a href="login.php">Si enfait j'ai un compte !</a>
</body>
</html>