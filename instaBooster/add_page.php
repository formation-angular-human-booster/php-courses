<?php
require_once 'pdo_connexion.php';
require_once 'functions/user-function.php';
require_once 'functions/image-function.php';
$user = isUserConnecter();
$errors = [];
if ( $_SERVER['REQUEST_METHOD'] === 'POST'){
   $errorAndLink = uploadImage();
   if(!empty($errorAndLink['filename'])){

       addBddImage($pdo, $errorAndLink['filename']);
       header('Location: homepage.php');
   } else {

       $errors = $errorAndLink['errors'];

   }
}
?>

<html>
<head>
    <?php
    include 'stylesheets.php';
    ?>
</head>
<body>
<?php
include 'nav.php';
?>
<h1>Formulaire d'ajout d'une image</h1>
<form method="post" enctype="multipart/form-data">
    <label>Image</label>
    <input class="form-control" type="file" name="photo"> <br>
    <label>Lieu de l'ajout</label>
    <input id="lieuAjout" class="form-control" type="text" name="lieu" placeholder="Lieu de l'ajout"><br>
    <input type="submit">
</form>
</body>

<?php

if(count($errors)>0){
    echo('<h2>Les erreurs : </h2>');
    foreach ($errors as $error){
        echo('<li>'.$error.'</li>');
    }
}
?>

<?php
 include 'javascripts.php';
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVMZW1FhHIzsLZGxg8hLpbtJWOeGCDo5Y&libraries=places">
</script>
<script src="assets/js/geolocator.js"></script>
</html>