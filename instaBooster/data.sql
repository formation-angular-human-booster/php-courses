-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: instaBooster
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `pseudo` varchar(45) NOT NULL,
                             `email` varchar(45) NOT NULL,
                             `prenom` varchar(45) NOT NULL,
                             `nom` varchar(45) NOT NULL,
                             `password` varchar(45) NOT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `id_UNIQUE` (`id`),
                             UNIQUE KEY `pseudo_UNIQUE` (`pseudo`),
                             UNIQUE KEY `email_UNIQUE` (`email`),
                             UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `pseudo`, `email`, `prenom`, `nom`, `password`) VALUES (1,'aurelien','aureliendelorme1@gmail.com','Aurélien','Delorme','43a733596dbef28dd14b0ff386948c36'),(13,'tot','toto@toto.fr','toto','toto','f71dbe52628a3f83a77ab494817525c6'),(14,'ezfdzef','qsdfe1@gmail.com','sdfdsf','qsdfdsf','20838a8df7cc0babd745c7af4b7d94e2'),(15,'admin','admin@admin.admin','admin','admin','21232f297a57a5a743894a0e4a801fc3');


--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photo` (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                       `file_name` varchar(45) NOT NULL,
                       `lieu_publi` varchar(45) NOT NULL,
                       `date_publication` datetime NOT NULL,
                       `nom_prenom_utilisateur` varchar(45) NOT NULL,
                       PRIMARY KEY (`id`),
                       UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `file_name`, `lieu_publi`, `date_publication`, `nom_prenom_utilisateur`) VALUES (1,'5e85f7243c427.png','Impasse Garenne, 63130 Royat, France','2020-04-02 14:31:00','admin admin');
