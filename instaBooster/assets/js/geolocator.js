
if (navigator.geolocation) {
   navigator.geolocation.getCurrentPosition(showPosition);
} else {
    alert('impossible votre navigateur ne permet pas la géolocalisation');
}

function showPosition(position) {

    url = 'https://api.opencagedata.com/geocode/v1/json?q='+position.coords.latitude+','+ position.coords.longitude + '&key=8d9044cbf590494f835969fc15e12dce';

    $.get(url, {}, function (data) {
       $('#lieuAjout').val(data.results[0].formatted);
    });
}