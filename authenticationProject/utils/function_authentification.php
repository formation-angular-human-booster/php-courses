<?php
session_start();
function getUserByUsername($pdo, $username)
{

    $res = $pdo->prepare('SELECT * FROM user WHERE username = :username');
    $res->execute([
        'username' => $username
    ]);
    return $res->fetch();
}

function validateFormulaire($pdo)
{
    $errors = [];

    if(empty($_POST['username'])){
        $errors[] = 'Veuillez entrer un username';
    }
    if(getUserByUsername($pdo, $_POST['username']) !== false){
        $errors[]  = 'Impossible username déjà existant';
    }

    if(empty($_POST['nom'])){
        $errors[] = 'Veuillez renseigner un nom';
    }

    if(empty($_POST['prenom'])){
        $errors[] = 'Veuillez renseigner un prénom';
    }

    if(empty($_POST['password'])){
        $errors[] = 'Veuillez renseigner un mot de passe';
    }

    return $errors;
}

function registerUser($pdo){
    try{
        $res = $pdo->prepare('INSERT INTO user   (username, nom, prenom, mot_de_passe ) 
          VALUES (:username, :nom, :prenom, :motdepasse)');
        $res->execute([
            ':username'=> $_POST['username'],
            ':nom'=> $_POST['nom'],
            ':prenom'=> $_POST['prenom'],
            ':motdepasse' => md5($_POST['password'])
        ]);

    } catch (\Exception $e) {
        var_dump($e);
        die();
    }

}

function validateLoginForm(){
    $errors = [];
    if(empty($_POST['username'])) {
        $errors[] = 'Veuillez saisir le username';
    }

    if(empty($_POST['password'])) {
        $errors[] = 'Veuillez saisir le password';
    }

    return $errors;
}

function connectUser($pdo) {
    $errors = [];
    $res = $pdo->prepare('SELECT * FROM user WHERE username = :username AND mot_de_passe = :mdp');
    $res->execute([
        'username'=> $_POST['username'],
        'mdp'=> md5($_POST['password'])
    ]);

    $res = $res->fetch();
    if(!$res) {
        $errors[] = 'Identifiants incorrecte';
        return $errors;
    } else {
        $_SESSION['user'] = $res;

    }
}
?>