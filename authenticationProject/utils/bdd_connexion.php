<?php
try {
    $host = 'mysql';
    $username = 'root';
    $password = 'tiger';
    $dbName = 'connexionUser';
    $pdo = new PDO(
        'mysql:host=' . $host . ';dbname=' . $dbName . ';charset=utf8',
        $username,
        $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (\Exception $e){
    echo('Impossible de se connecter !!');
    var_dump($e);
}
?>