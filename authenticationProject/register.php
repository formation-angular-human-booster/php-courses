<?php
    require_once 'utils/include.php';
    $errors = [];
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $errors = validateFormulaire($pdo);
        if(count($errors) === 0){
            registerUser($pdo);
            header('Location: login.php');
        }
    }
?>
<html>
<head>
</head>

<body>
<h1>M'enregistrer</h1>
<form method="post">
    <label>
        Username
    </label>
    <input name="username" required type="text" placeholder="Pseudo">
    <label>
        Nom
    </label>
    <input name="nom" type="text"  placeholder="Nom">
    <label>
        Prénom
    </label>
    <input name="prenom" type="text"  placeholder="Prénom">

    <label>
        Password
    </label>
    <input name="password" type="password"  placeholder="Mot de passe">
    <input type="submit">
</form>
    <a href="login.php">Accèder au login</a>
<ul>
<?php
    if(count($errors)>0){
    echo('<h2>Les erreurs du formulaire : </h2>
    <ul>');
    foreach ($errors as $err){
        echo('<li>'.$err.'</li>');
    }
echo('</ul>');
    }
?>
</ul>
</body>
</html>