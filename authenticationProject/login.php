<?php
    require_once 'utils/include.php';
    $errors = [];
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $errors = validateLoginForm();
        if(count($errors) === 0){
            $errors = connectUser($pdo);
        }
    }
?>
<html>
<head>

</head>
<body>
<h1>Veuillez vous connecter</h1>
<form method="post">
    <label>
        Username
    </label>
    <input type="text" name="username" placeholder="Veuillez saisir votre username">

    <label>
        Password
    </label>
    <input type="password" name="password" placeholder="Veuillez saisir votre password">
    <input type="submit">
</form>

<?php
if(count($errors)>0){
    echo('<h2>Les erreurs du formulaire : </h2>
    <ul>');
    foreach ($errors as $err){
        echo('<li>'.$err.'</li>');
    }
    echo('</ul>');
}
?>

<a href="register.php">J'ai pas de compte je veux m'enregistrer</a>
</body>
</html>
