<h1>Exercice 1</h1>
<?php
// Solution 1

function reverseStringMethod1($string) {
    $str = str_split($string);
    $str = array_reverse($str);
    return implode('', $str);
}

function reverseStringMethod2($string) {
    $reverse = [];
    for ($i = strlen($string)-1; $i>=0; $i--) {
        $reverse[] = $string[$i];
    }
    return implode('', $reverse);
}

function reverseStringMethod3($string) {
    return strrev($string);
}

var_dump(reverseStringMethod1('Bonjour ! '));

/*
echo '<br>';

// Solution 2
$str = 'Bonjour !';
$reverse = [];
for ($i = strlen($str) - 1; $i >= 0 ; $i--) {
    $reverse[] = $str[$i];
}
echo implode('', $reverse).'<br>';

// Solution 3

echo(strrev($str));*/

echo(strrev('Ici ! Ici ! C\'est Montferrand'));
