<?php
    class Voiture extends VehiculeModel implements VoitureInterface {
        private $isDecapotable;

        public function __construct($vitesseMax, $vitesseActuelle,
                                    $couleur, $marque, $nbCheveaux,
                                    $isDecapotable)
        {
            echo('Je construit dabord l\'objet parent <br>');

            parent::__construct($vitesseMax, $vitesseActuelle,
                $couleur, $marque, $nbCheveaux);

            echo('Je finalise par la construction des éléments propre
            à une voiture<br>');
            $this->isDecapotable = $isDecapotable;
        }

        public function claxonner()
        {
            echo('BIIIPPPP');
        }
    }
?>