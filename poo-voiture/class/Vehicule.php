<?php
abstract class Vehicule implements VehiculeInterface {

    public static $name = 'Aurélien';
    private $vitesseMax;
    private $vitesseActuelle;
    private $couleur;
    private $marque;
    private $nbCheveaux;

    public function __construct($vitesseMax, $vitesseActuelle,
                                $couleur, $marque, $nbCheveaux){
        echo('Je construit un véhicule !');
        $this->vitesseMax = $vitesseMax;
        $this->vitesseActuelle = $vitesseActuelle;
        $this->couleur = $couleur;
        $this->marque = $marque;
        $this->nbCheveaux = $nbCheveaux;
    }

    public function accelerer(){
        echo('J\'accélére !!!!');
        if($this->vitesseActuelle < $this->vitesseMax - 10){
            $this->vitesseActuelle = $this->vitesseActuelle + 10;
            // Vitesse actuelle 100 / max 200
        } elseif($this->vitesseActuelle < $this->vitesseMax) {
            $this->vitesseActuelle = $this->vitesseMax;
        } else {
            echo('Je suis à fond déjà ...');
        }
        return $this->vitesseActuelle;
    }

    public function freiner(){
        echo('Je freine !!');
        if($this->vitesseActuelle == 0){
            echo('Je peux pas je suis arrété');
        } elseif($this->vitesseActuelle-10 < 0 ) {
            $this->vitesseActuelle = 0;
        } else {
            $this->vitesseActuelle -= 10;
        }
        return $this->vitesseActuelle;
    }


    public function displayCarDetail(){
        echo('La voiture '. $this->couleur.' de la marque '
            .$this->marque.' roule à '.$this->vitesseActuelle .' Km/H');
    }

    public function __destruct(){
        echo('Objet détruit');
    }
}