<?php
interface VehiculeInterface {
    public function accelerer();
    public function freiner();
    public function displayCarDetail();
    public function __destruct();
}