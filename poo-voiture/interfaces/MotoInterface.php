<?php
interface MotoInterface extends VehiculeInterface {
    public function __construct($vitesseMax, $vitesseActuelle, $couleur, $marque, $nbCheveaux);

}