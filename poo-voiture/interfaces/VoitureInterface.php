<?php
interface VoitureInterface extends VehiculeInterface {
    public function __construct($vitesseMax, $vitesseActuelle,
                                $couleur, $marque, $nbCheveaux,
                                $isDecapotable);
    public function claxonner();
}