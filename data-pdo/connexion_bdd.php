<?php
// Jessaie de me connecter à ma BDD
try {
    // Ici je cré un nouvel
    $bdd = new PDO('mysql:host=database;port=3306;dbname=demo_pdo;charset=utf8',
        'root', 'tiger');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Ici le cas ou je n'ai pas réussi à me connecter j'affiche une erreur
} catch (PDOException $exception){
    var_dump($exception);
    die();
}
?>
<html>
<head>

</head>
<body>
<table>
    <thead>
    <td>Nom</td>
    <td>Ordre affichage</td>
    </thead>
    <tbody>

    <?php
    // Je demande ici à mon serveur de faire la requête SELECT * FROM category et je lui
    // demande d'ordonner mes resultat en fonction de display_order
    $requete = $bdd->query("SELECT * FROM category 
ORDER BY display_order DESC;");

    // Je fais une boucle qui affichera une ligne dans mon tableau pour chacun de mes
    // resultats
        while ($result = $requete->fetch()){
            echo('<tr>
            <td>'.$result['name'].'</td>
                      <td>'.$result['display_order'].'</td>
            </tr>');

        }

    ?>
    
    </tbody>
</table>
</body>
</html>
<?php
/* var_dump($result);

$result = $requete->fetch();

var_dump($result);

die();
echo('Connecté !');*/
?>
