<?php
class Session{

    private $attributs = [];

    public function __construct(){
        echo('Je reconstruit mon objet à partir de la session');
        if(isset($_SESSION['session'])){
            $textSession = $_SESSION['session'];
            $object = unserialize($textSession);
            $this->attributs = $object->getAttributs();
        }
    }

    public function getAttributs(){
        return $this->attributs;
    }

    public function __set($key, $valeur){
        $this->attributs[$key] = $valeur;
    }

    public function __get($key){
        return $this->attributs[$key];
    }

    public function __destruct(){
        echo('Je passe ici pour mettre tous les attributs dans ma session web');
        $_SESSION['session'] = serialize($this);
    }

    public function logout(){
        $this->attributs = [];
        session_destroy();
    }
}