<?php
class Animal {
    protected $nom;
    public function __construct($nom)
    {
        //echo('Je construit un nouvel animal');
        $this->nom = $nom;
    }
    public function recevoirNom($nom){
        $this->nom = $nom;
    }

    public function dormir() {
        echo('Je dors');
    }
}