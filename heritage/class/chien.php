<?php
class Chien extends Animal {

    public $race;
    public $description;


    /**
     * Chien constructor.
     */
    public function __construct($nom, $race, $description)
    {
        parent::__construct($nom);
        $this->race = $race;
        $this->description = $description;
    }

    public function aboyer(){
        echo('OUAF !! ');
    }

    public function dormir() {
        parent::dormir();
        echo('mais je suis un chien');
    }

    public function attribuerNom($nom){
        $this->nom = $nom;
    }
}