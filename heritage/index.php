<?php
require_once 'class/animal.php';
require_once 'class/chien.php';

$chien = new Chien('Neymar', 'Epagneul', 'test');
$chien->aboyer();
$chien->recevoirNom('Neymar');
var_dump($chien);

?>

<h1>Appel de la fonction dormir depuis un animal</h1>

<?php
    $animal = new Animal('neymar');
    echo($animal->dormir());
?>

<h2>Appel de la fonction dormir depuis un chien</h2>
<?php
    $chien = new Chien('Loulou', 'Chiwawa', 'Chien de fragile');
    $chien->dormir();
    var_dump($chien);

    ?>