<?php
class Ordinateur{
    private $nom;
    private $marque;
    private $couleur;

    public function __construct($nom, $marque, $couleur){
        $this->nom = $nom;
        $this->marque = $marque;
        $this->couleur = $couleur;
    }

    public function getNom(){
        return $this->nom;
    }

     function additionner($entier1, $entier2){
        return $entier1 + $entier2;
    }


}
?>