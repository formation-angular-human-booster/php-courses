<?php
require 'autoload.php';

if(empty($_GET['controller']) || empty($_GET['action'])){
    header('Location: index.php?controller=equipe&action=homepage');
}

if($_GET['controller'] == 'security'){
    $controller = new SecurityController();
    if($_GET['action'] == 'login'){
        $controller->login();
    }
    if($_GET['action'] == 'logout'){
        $controller->logout();
    }
}

if($_GET['controller'] == 'equipe'){
    $controller = new EquipeController();
    if($_GET['action'] == 'homepage'){
        $controller->homePage();
    }
    if($_GET['action'] == 'add'){
        if(!empty($_SESSION['user'])){
            $controller->add();
        } else {
            echo('Interdit !!!!!');
        }

    }

    if($_GET['action'] == 'edit' && isset($_GET['id'])){

        if(!empty($_SESSION['user'])){
            $controller->edit($_GET['id']);
        } else {
            echo('Interdit !!!!!');
        }
    }

    if($_GET['action'] == 'delete' && isset($_GET['id'])){
        if(!empty($_SESSION['user'])){
            $controller->delete($_GET['id']);
        } else {
            echo('Interdit !!!!!');
        }
    }
}
?>