<html>
<head>

</head>
<body>
<?php
require 'Vue/parts/menu.php';
?>
<h1>Editer une équipe</h1>

<form method="post" enctype="multipart/form-data">
    <label>Nom</label>
    <input type="text" value="<?php echo($equipe->getNom());?>" placeholder="Nom de l'équipe" name="nom">

    <label>But mis</label>
    <input type="number" value="<?php echo($equipe->getButMis());?>" placeholder="Nombre de buts marqué" name="but_mis">

    <label>But encaissés</label>
    <input type="number"  value="<?php echo($equipe->getButPris());?>"  placeholder="Nombre de buts encaissés" name="but_pris">`

    <label>Point</label>
    <input type="number" value="<?php echo($equipe->getPoint());?>" placeholder="Nombre de points" name="points">

    <label>Image</label>
    <input type="file" name="logo">

    <?php
        if(!is_null($equipe->getImage())){
            echo('<img src="public/logo/'.$equipe->getImage().'">');
        }
    ?>
    <input type="submit">

    <?php
    require 'Vue/parts/form_errors.php';
    ?>
</form>
</body>
</html>