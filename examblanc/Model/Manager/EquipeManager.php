<?php
class EquipeManager extends DbManager {


    public function findOrderedTeam(){
        $equipes = [];

        $req = $this->bdd->prepare("SELECT * FROM equipe
        ORDER BY point DESC, but_pris ASC, but_mis DESC");
        $req->execute();
        $resultats = $req->fetchAll();

        foreach ($resultats as $resultat){
            $equipes[] = new Equipe($resultat['nom'], $resultat['but_mis'],
                $resultat['but_pris'], $resultat['point'], $resultat['image_link'], $resultat['id']);

        }

        return $equipes;
    }


    public function getOne($id){
        $equipe = null;
        $req = $this->bdd->prepare('SELECT * FROM equipe WHERE id = :id');

        $req->execute([
            'id'=> $id
        ]);

        $resultat = $req->fetch();

        if($resultat){
            $equipe = new Equipe($resultat['nom'], $resultat['but_mis'],
                $resultat['but_pris'], $resultat['point'], $resultat['image_link'], $resultat['id']);
        }

        return $equipe;

    }

    public function getAll(){
        // TODO
    }

    public function delete(Equipe $equipe){
        $req = $this->bdd->prepare('DELETE FROM equipe WHERE id = :id');

        $req->execute(['id'=> $equipe->getId()]);
    }

    public function edit(Equipe $equipe){
        $req = $this->bdd->prepare("UPDATE equipe 
    SET nom = :nom, but_mis = :butMis, but_pris = :butPris,
        point = :point, image_link = :imageLink WHERE id = :id");

        $req->execute([
            'nom'=> $equipe->getNom(),
            'butMis'=> $equipe->getButMis(),
            'butPris'=> $equipe->getButPris(),
            'point'=> $equipe->getPoint(),
            'imageLink'=> $equipe->getImage(),
            'id'=> $equipe->getId()
        ]);
    }

    public function getByName($name){
        $equipe = null;
        $req = $this->bdd->prepare('SELECT * FROM equipe WHERE nom = :nom');

        $req->execute([
            'nom'=> $name
        ]);

        $resultat = $req->fetch();
        if($resultat){
            $equipe = new Equipe($resultat['nom'], $resultat['but_mis'],
                $resultat['but_pris'], $resultat['point'], $resultat['image_link'], $resultat['id']);
        }
        return $equipe;
    }


    public function add(Equipe $equipe){
       $req = $this->bdd->prepare("INSERT INTO equipe 
    (nom, but_mis, but_pris, point, image_link) 
    VALUE (:nom, :butMis, :butPris, :point, :imageLink)");

       $req->execute([
           'nom'=> $equipe->getNom(),
           'butMis'=> $equipe->getButMis(),
           'butPris'=> $equipe->getButPris(),
           'point'=> $equipe->getPoint(),
           'imageLink'=> $equipe->getImage()
       ]);
    }



}
?>