<?php
class Equipe {
    private $id;
    private $nom;
    private $butMis;
    private $butPris;
    private $point;
    private $image;


    public function __construct($nom, $butMis, $butPris, $point, $image = null, $id=null)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->butMis = $butMis;
        $this->butPris = $butPris;
        $this->point = $point;
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getButMis()
    {
        return $this->butMis;
    }

    /**
     * @param mixed $butMis
     */
    public function setButMis($butMis): void
    {
        $this->butMis = $butMis;
    }

    /**
     * @return mixed
     */
    public function getButPris()
    {
        return $this->butPris;
    }

    /**
     * @param mixed $butPris
     */
    public function setButPris($butPris): void
    {
        $this->butPris = $butPris;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     */
    public function setPoint($point): void
    {
        $this->point = $point;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }


}