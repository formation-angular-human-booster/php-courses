<?php
class EquipeController{

    private $equipeManager;

    public function __construct(){
        $this->equipeManager = new EquipeManager();
    }

    public function homePage(){
        $equipes = $this->equipeManager->findOrderedTeam();
        require 'Vue/equipe/homepage.php';
    }

    public function add(){
        $errors = [];
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $imageFileName = null;

            $errors = $this->getFormErrors();



            if(count($errors) == 0){

                $upload = $this->uploadImage();
                $errors = $upload['errors'];
                $imageFileName = $upload['filename'];


                if(count($errors) == 0){
                    $equipe = new Equipe($_POST['nom'], $_POST['but_mis'], $_POST['but_pris'], $_POST['points'], $imageFileName);
                    $this->equipeManager->add($equipe);
                    header('Location: index.php?controller=equipe&action=homepage');
                }

            }

        }

        require 'Vue/equipe/add.php';

    }

    public function edit($id){

        $errors = [];
        $equipe = $this->equipeManager->getOne($id);

        if(is_null($equipe)){
            echo('Erreur equipe introuvable !');
            die();
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
           $errors = $this->getFormErrors($id);
           if(count($errors) == 0){
               if($_FILES['logo']['size'] != 0){
                   $upload = $this->uploadImage();
                   $errors = $upload['errors'];
                   $imageFileName = $upload['filename'];
               } else {
                   $imageFileName = $equipe->getImage();
               }


                if(count($errors) == 0){
                    $equipe = new Equipe($_POST['nom'], $_POST['but_mis'], $_POST['but_pris'], $_POST['points'], $imageFileName, $id);
                    $this->equipeManager->edit($equipe);

                    header('Location: index.php?controller=equipe&action=homepage');

                }
           }
        }
        require 'Vue/equipe/edit.php';
    }

    public function delete($id){

        $equipe = $this->equipeManager->getOne($id);
        if(is_null($equipe)){
            var_dump('Tu n\'as pas le droit de faire ça utilise un lien !');
            die();
        } else {
            $this->equipeManager->delete($equipe);
            header('Location: index.php?controller=equipe&action=homepage');
        }
    }

    private function getFormErrors($id = null){
        $errors = [];
        if(empty($_POST['nom'])){
            $errors[] = 'Veuillez saisir un nom';
        }

        if($_POST['but_mis'] == ''){
            $errors[] = 'Veuillez saisir le nombre de buts marqués';
        }

        if($_POST['but_pris'] == ''){
            $errors[] = 'Veuillez saisir le nombre de bot encaissés';
        }

        if($_POST['points'] == null){
            $errors[] = 'Veuillez saisir le nombre de points';
        }

        $equipe = $this->equipeManager->getByName($_POST['nom']);


        if(!is_null($equipe) && $equipe->getId() != $id){
            $errors[] = 'Une équipe avec ce nom existe déjà !';
        }

        return $errors;
    }

    private function uploadImage(){
        $extensionAllowed = ['image/jpeg', 'image/png'];
        $errors = [];
        $imageFileName = null;

        if($_FILES['logo']['size'] != 0){
            $image = $_FILES['logo'];
            if($image['size']>10000000){
                $errors[] = 'L\'image du club est trop grosse';
            }

            if(!in_array($image['type'], $extensionAllowed)){
                $errors[] = 'Impossible d\'uploader ce type de fichier';
            }

            if($image['error']!=0){
                $errors[] = 'Une erreur inconnue s\'est produite lors de l\'upload';
            }


            if(count($errors) == 0){
                $imageFileName = uniqid().'.'.explode('/', $image['type'])[1];
                move_uploaded_file($image['tmp_name'], 'public/logo/'. $imageFileName);

            }
        }

        return ['filename'=>$imageFileName, 'errors'=> $errors];
    }
}
?>