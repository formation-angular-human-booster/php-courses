<?php
class SecurityController {

    private $utilisateurManager;

    public function __construct(){
        $this->utilisateurManager = new UtilisateurManager();
    }

    public function login(){

        $errors = [];

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            if(empty($_POST['username'])){
                $errors[] = 'Veuillez saisir un username';
            }

            if(empty($_POST['password'])){
                $errors[] = 'Veuillez saisir un password';
            }

            if(count($errors) == 0){
                $resultat = $this->utilisateurManager->login($_POST['username'], $_POST['password']);

                if(!is_null($resultat)){
                    $_SESSION['user'] = serialize($resultat);
                   header('Location: index.php?controller=equipe&action=homepage');
                } else {
                    $errors[] = 'Les identifiants sont incorrectes !';
                }
            }

        }

        require 'Vue/security/login.php';
    }

    public function logout(){
        session_destroy();
        header('Location: index.php?controller=security&action=login');
    }
}