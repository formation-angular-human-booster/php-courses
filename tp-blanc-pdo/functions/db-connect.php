<?php
try {
    // Variables de connexion à notre BDD (url, nom de la db user password)
    $host = 'database';
    $dbName = 'tp_blanc';
    $user = 'root';
    $password = 'tiger';

    // On cré un nouvel objet PDO. Pour créer un objet il nous faut
    // une chaine de connexion, le user et le password
    $pdo = new PDO(
        'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);

    // Ici je demande à PDO d'afficher toutes mes erreurs
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e){

    // Cas ou ça se passe mal il arrive pas à se connecter, on affiche la cause
    // du refus de connexion et on stop tout
    var_dump($e);
    die();
}
?>