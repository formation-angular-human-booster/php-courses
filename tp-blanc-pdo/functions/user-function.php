<?php

function checkUser(){
    if(!array_key_exists("username", $_SESSION)){
        header("Location: login.php");
    }
}

function getUtilisateurByUsername($pdo, $username){
    $request =
        $pdo->prepare("SELECT * FROM utilisateur WHERE nom_utilisateur = :username ");
    $request->execute(['username'=> $username]);
    $result = $request->fetch();
    $request->closeCursor();
    return $result;
}