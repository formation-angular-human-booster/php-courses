<?php
    function addArticle($pdo, $titre, $type, $contenu, $lienImage){
        try {
            $request = $pdo->prepare("INSERT INTO annonce (titre, type, image_lien, contenu, journaliste)
        VALUE (:titre, :type, :image_lien, :contenu, :journaliste)");

            $request->execute(
                [
                    'titre'=> $titre,
                    'type'=> $type,
                    'image_lien'=> $lienImage,
                    'contenu'=> $contenu,
                    'journaliste'=> $_SESSION['nom']. ' '. $_SESSION['prenom']
                ]);
        } catch (\PDOException $e){
            var_dump($e);
        }
    }

    function getAllArticles($pdo){
        $request = $pdo->prepare("SELECT * FROM annonce");
        $request->execute();

        $resultat = $request->fetchAll();

        return $resultat;
    }

    function deleteArticle($pdo, $idToDelete){
        $request = $pdo->prepare('DELETE FROM annonce WHERE id = :id');
        $request->execute(['id'=> $idToDelete]);
    }

    function getOneArticle($pdo, $id){
        $request = $pdo->prepare("SELECT * FROM annonce WHERE id = :id");
        $request->execute(['id'=> $id]);

        return $request->fetch();
    }

    function editArticle($pdo, $titre, $type, $contenu, $imageLink, $id){
        $request = $pdo->prepare("UPDATE annonce SET titre = :titre, type = :type,
                   image_lien = :image, contenu = :contenu WHERE id = :id");

        $request->execute([
            'titre'=> $titre,
            'type'=> $type,
            'contenu'=> $contenu,
            'image'=> $imageLink,
            'id'=> $id
        ]);
    }
?>