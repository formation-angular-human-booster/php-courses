<?php
    require '../functions/db-connect.php';
    require '../functions/article-function.php';
    $id = $_GET['id'];
    deleteArticle($pdo, $id);
    header("Location: admin.php");
?>