<?php
// session_start();
require '../functions/db-connect.php';

require "../functions/user-function.php";
$errors = [];
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(empty($_POST['username'])){
        $errors[] = 'Veuillez saisir un nom d\'utilisateur';
    }

    if(empty($_POST['password'])){
        $errors[] = 'Veuillez saisir un mot de passe';
    }

    $user = getUtilisateurByUsername($pdo, $_POST['username']);

    if(!$user){
        $errors[] = 'Cet utilisateur n\'existe pas';
    }

    if(password_verify($_POST['password'], $user['mot_de_passe'])){
        $_SESSION['username'] = $user['nom_utilisateur'];
        $_SESSION['nom'] = $user['nom'];
        $_SESSION['prenom'] = $user['prenom'];
        header("Location: admin.php");
    } else {
        $errors[] = 'Identifiants incorrectes';
    }



}
?>
<html>
<head>
    <?php
    include "../global/global-style.php";
    ?>
</head>

<body>
<?php
include "../global/header.php";
?>
<div class="text-center">
    <div class="col-md-6 offset-3">
<form method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Nom d'utilisateur</label>
        <input required type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nom d'utilisateur">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input required type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>


<br>
    <?php
        foreach ($errors as $error){
            echo(' <small id="emailHelp" class="form-text text-muted">'.$error.'</small><br>');
        }
    ?>

</form>
    </div>
</div>
<?php
include "../global/global-script.php";
?>
</body>
</html>
