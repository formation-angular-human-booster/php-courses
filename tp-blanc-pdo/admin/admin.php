<?php
    require '../functions/db-connect.php';
    require '../functions/article-function.php';
    require "../functions/user-function.php";
    checkUser();
    $articles = getAllArticles($pdo);

?>
<html>
<head>
    <?php
        include "../global/global-style.php";
    ?>
</head>
<body>
<div class="container">
<?php
    include "../global/header.php";
    include "menu.php";
?>
<h1>Les annonces</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titre</th>
            <th scope="col">Type</th>
            <th scope="col">Contenu</th>
            <th scope="col">Journaliste</th>
            <th scope="col">Image</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $string = '';
            foreach ($articles as $article){
                $string .= '<tr>
            <th scope="row">'.$article['id'].'</th>
            <td>'.$article['titre'].'</td>
            <td>'.$article['type'].'</td>
            <td>'.$article['contenu'].'</td>
            <td>'.$article['journaliste'].'</td>
            <td>';

                if(!is_null($article['image_lien'])){
                    $string .=   '<img class="img-thumbnail" src="../public/images/uploads/'.$article['image_lien'].'"/>';
                }

            $string .= '<td>
                <a href="delete-annonce.php?id='.$article['id'].'">Supprimer l\'annonce</a>
                 <a href="article-edit.php?id='.$article['id'].'">Editer l\'annonce</a>
            </td>
        </tr>';
            }

            echo($string);
        ?>

        </tbody>
    </table>
<?php
    include "../global/global-script.php";
?>
</div>
</body>
</html>
