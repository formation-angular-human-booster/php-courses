<?php
require "../functions/db-connect.php";
require "../functions/article-function.php";
require "../functions/user-function.php";
checkUser();

$article = getOneArticle($pdo, $_GET['id']);

if(!$article){
    header("Location: admin.php");
}

$errors = [];


if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if(empty($_POST['titre'])){
        $errors[] = 'Veuillez ajouter un titre';
    }

    if(empty($_POST['type'])){
        $errors[] = 'Veuillez ajouter un type';
    }

    if(empty($_POST['contenu'])){
        $errors[] = 'Veuillez ajouter un contenu';
    }

    $allowedAnnonceType = ['politique', 'faits-divers', 'sport', 'autres'];

    if(!in_array($_POST['type'], $allowedAnnonceType)){
        $errors[] = 'Ne change pas les types dans la console !';
    }


    $imageLink = null;
    if($_FILES['image_annonce']['size']!=0){
        $allowedTypes = ['image/jpeg', 'image/png'];
        $maxSize = 1000000;

        if(!in_array($_FILES['image_annonce']['type'], $allowedTypes)){
            $errors[] = 'Le fichier n\'est pas au bon format';
        }
        if($_FILES['image_annonce']['size']>$maxSize){
            $errors[] = 'Le fichier est trop lourd';
        }

        if(count($errors) == 0){
            $imageLink = uniqid().'.'.explode('/', $_FILES['image_annonce']['type'])[1];
            move_uploaded_file($_FILES['image_annonce']['tmp_name'], '../public/images/uploads/'. $imageLink);
        }
    } else {
        $imageLink = $article['image_lien'];
    }

    if(count($errors) == 0){
        editArticle($pdo, $_POST['titre'], $_POST['type'], $_POST['contenu'], $imageLink, $article['id']);
        header("Location: admin.php");
    }
}




?>
<html>
<head>
    <?php
    include "../global/global-style.php";
    ?>
</head>
<body>
<div class="container">
    <?php
    include "../global/header.php";
    include "menu.php";
    ?>
    <h1>Editer l'article <?php echo($article['titre']) ?></h1>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="exampleInputEmail1">Titre</label>
            <input value="<?php echo($article['titre']);?>" required type="text" name="titre" class="form-control" placeholder="Titre de l'annonce"/>

        </div>
        <div class="form-group">
            <label for="type">Titre de l'annonce</label>
            <select required id="type" class="form-control" name="type">
                <option <?php if($article['type'] == 'politique')echo('selected');?> value="politique">Politique</option>
                <option  <?php if($article['type'] == 'faits-divers')echo('selected');?> value="faits-divers">Faits divers</option>
                <option   <?php if($article['type'] == 'sport')echo('selected');?> value="sport">Sport</option>
                <option <?php if($article['type'] == 'autres')echo('selected');?>  value="autres">Autres</option>
            </select>
        </div>

        <div class="form-group">
            <label for="contenu">Contenu de l'annonce</label>
            <textarea class="form-control" id="contenu" name="contenu"><?php echo($article['contenu']);?></textarea>
        </div>

        <div class="form-group">
            <?php
            if(!is_null($article['image_lien'])){
                echo('<img class="img-thumbnail" src="../public/images/uploads/'.$article['image_lien'].'"/>');
            } else {
                echo('Aucune image pour le moment');
            }
            ?>

            <label for="image">Ajoutes une image pour modifier</label>
            <input class="form-control" type="file"  accept="image/png, image/jpeg" name="image_annonce">

        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>


        <br>
        <?php
        foreach ($errors as $error){
            echo(' <small id="emailHelp" class="form-text text-muted">'.$error.'</small><br>');
        }
        ?>
    </form>
    <?php
    include "../global/global-script.php";
    ?>
</div>
</body>
</html>
