<html>
<head>
    <link rel="stylesheet" href="public/css/annonce.css">
    <?php
    include "global/global-style.php";
    ?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="public/images/logo.png">
        </div>
    </div>

    <h1>Les news !</h1>

    <div class="row">
        <?php
        require 'functions/db-connect.php';
        require 'functions/article-function.php';
        $articles = getAllArticles($pdo);

        foreach ($articles as $article) {
            $contenu = substr($article['contenu'], 0, 20);

            if(strlen($article['contenu'])>20){
                $contenu.= ' ...';
            }


            echo('<div class="card '.$article['type'].'" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.$article['titre'].'</h5>
    <p class="card-text">'.$contenu.'</p>
    <a href="annonce.php?id='.$article['id'].'" class="btn btn-primary">Voir en détail</a>
  </div>
</div>');
        }
        ?>
    </div>



    <?php
    include "global/global-script.php";
    ?>
</div>
</body>
</html>
