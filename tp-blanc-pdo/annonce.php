<html>
<head>
    <link rel="stylesheet" href="public/css/annonce.css">
    <?php
    include "global/global-style.php";
    ?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="public/images/logo.png">
        </div>
    </div>



        <?php
        require 'functions/db-connect.php';
        require 'functions/article-function.php';
        $article = getOneArticle($pdo, $_GET['id']);
        ?>

    <div class="col-md-12 text-center <?php echo($article['type']);?>">
        <a href="index.php">
            <button class="btn btn-success">Retour</button>
        </a>
        <h1>L'annonce <?php echo($article['titre']);?> !</h1>

        <?php
        if(!is_null($article['image_lien'])){
            echo('<img src="public/images/uploads/'.$article['image_lien'].'"/>');
        }
        ?>

        <div class="col-md-12 text-justify"><?php echo($article['contenu']);?></div>
    </div>




    <?php
    include "global/global-script.php";
    ?>
</div>
</body>
</html>
