<?php
class MonstreCarnivoreException extends Exception {
    public function verifierNourriture($nouriture) {
        if($nouriture === 'salade'){
            throw new Exception('Un monstre ne mange pas de salade');
        }
    }
}