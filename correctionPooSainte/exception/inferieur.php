<?php
function inferieurA100($chiffre)
{
    if ($chiffre>100)
    {
        throw new Exception('Le nombre doit être < 100');
    }

    return $chiffre;
}

try // Nous allons essayer d'effectuer les instructions situâˆšées dans ce bloc.
{
    echo inferieurA100(12), '<br />';
    echo inferieurA100(120), '<br />';
} catch (Exception $e){
    // traitement de l'erreur
    echo $e->getMessage();
}

echo '<br>Fin du script';
echo('<br>Suite du script ....');