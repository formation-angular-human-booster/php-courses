<?php
class MonstreCarnivore extends Monstre implements MonstreCarnivoreInterface {
    public static $name = 'Monstre Carnivore';
    public $couleur;
    public $poids;
    public $sexe;
    public $customAttribute = [];


    public function __construct()
    {
    }

    public function __sleep()
    {
        return ['couleur', 'poids'];
    }

    public function __wakeup()
    {
        $this->customAttribute = [];
    }

    public function __unset($name)
    {
        unset($this->customAttribute[$name]);
    }

    public function __isset($name)
    {
       return isset($this->customAttribute[$name]);
    }

    public function __set($name, $value)
    {
        $this->customAttribute[$name] = $value;
    }

    public function __get($name) {
        return $this->customAttribute[$name];
    }

    public function attaquer() {
        parent::attaquer();
        $aleatoire = rand(10,20);
        echo('jattaque avec une force de '.$aleatoire.'<br>');
        return $aleatoire;
    }

    public function mangerViande($viande)
    {
        $verification = new MonstreCarnivoreException();
        try {
            $verification->verifierNourriture($viande);
            echo('Je mange de la viande de '. $viande);
        } catch (\Exception $e){
            echo($e->getMessage());
        }
    }
}