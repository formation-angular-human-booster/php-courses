<?php
class PersonnageGentil extends Personnage {
    public static $name = 'Personnage gentil';
    public $soinMinimum;
    public $soinMaximum;

    /**
     * PersonnageGentil constructor.
     * @param $soinMinimum
     * @param $soinMaximum
     */
    public function __construct($soinMinimum, $soinMaximum)
    {
        $this->soinMinimum = $soinMinimum;
        $this->soinMaximum = $soinMaximum;
    }

    public function soigner() {
        return rand($this->soinMinimum, $this->soinMaximum);
    }
}
?>