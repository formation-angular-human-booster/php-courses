# celine_rechatin_poo_exam
Donner 5 méthodes magiques que vous avez étudié en PHP OO. Expliquez les éléments
déclencheurs de l’exécution de la méthode magique:
__construct
elle permet de construire l'objet
__set
Elle permet de détecter la modification d’un attribut qui n’existe pas ou
qui est privé.
Elle prend en paramètre deux arguments : le nom  et  la valeur de l'attribut.

__get
Elle permet de détecter l’accession à un attribut qui n’existe pas.
Elle prend en paramètre le nom de l’attribut.

__isset
 On appel la fonction isset sur un attribut, elle prend en parametre le nom de l'attribut,
 elle renvoie un booléen.

 __unset
Elle est appelé quand on appel la fonction unset sur un attribut,
elle renvoie un booléen

__destruct elle deruit l'objet
 Elle est appelée dès qu'il n'y a plus de référence sur un objet donné, ou dans n'importe quel ordre pendant la séquence d'arrêt.


 Pour chacune des méthodes ci-dessus proposez un exemple de script qui appellera la méthode de manière implicite 
 ---------------------------------------------------------------------------
 class MaClasse
{
  public function __construct()
  {
    echo 'Construction de MaClasse';
  }
  
  public function __destruct()
  {
    echo 'Destruction de MaClasse';
  }
} $obj = new Ma;Classe
----------------------------------------------------------------------------
<?php
class MaClasse
{
  private $attributs = [];
  private $unAttributPrive;
  
  public function __get($nom)
  {
    if (isset($this->attributs[$nom]))
    {
      return $this->attributs[$nom];
    }
  }
  
  public function __set($nom, $valeur)
  {
    $this->attributs[$nom] = $valeur;
  }
  
  public function afficherAttributs()
  {
    echo '<pre>', print_r($this->attributs, true), '</pre>';
  }
}

$obj = new MaClasse;

$obj->attribut = 'Simple test';
$obj->unAttributPrive = 'Autre simple test';

echo $obj->attribut;
echo $obj->autreAtribut;
-------------------------------------------------------------------------------
<?php
class MaClasse
{
  private $attributs = [];
  private $unAttributPrive;
  
  public function __set($nom, $valeur)
  {
    $this->attributs[$nom] = $valeur;
  }
  
  public function __get($nom)
  {
    if (isset($this->attributs[$nom]))
    {
      return $this->attributs[$nom];
    }
  }
  
  public function __isset($nom)
  {
    return isset($this->attributs[$nom]);
  }
}

$obj = new MaClasse;

$obj->attribut = 'Simple test';
$obj->unAttributPrive = 'Autre simple test';

if (isset($obj->attribut))
{
  echo 'L\'attribut <strong>attribut</strong> existe !<br />';
}
else
{
  echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
}

if (isset($obj->unAutreAttribut))
{
  echo 'L\'attribut <strong>unAutreAttribut</strong> existe !';
}
else
{
  echo 'L\'attribut <strong>unAutreAttribut</strong> n\'existe pas !';
}
-------------------------------------------------------------------
<?php
class MaClasse
{
  private $attributs = [];
  private $unAttributPrive;
  
  public function __set($nom, $valeur)
  {
    $this->attributs[$nom] = $valeur;
  }
  
  public function __get($nom)
  {
    if (isset($this->attributs[$nom]))
    {
      return $this->attributs[$nom];
    }
  }
  
  public function __isset($nom)
  {
    return isset($this->attributs[$nom]);
  }
  
  public function __unset($nom)
  {
    if (isset($this->attributs[$nom]))
    {
      unset($this->attributs[$nom]);
    }
  }
}

$obj = new MaClasse;

$obj->attribut = 'Simple test';
$obj->unAttributPrive = 'Autre simple test';

if (isset($obj->attribut))
{
  echo 'L\'attribut <strong>attribut</strong> existe !<br />';
}
else
{
  echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
}

unset($obj->attribut);

if (isset($obj->attribut))
{
  echo 'L\'attribut <strong>attribut</strong> existe !<br />';
}
else
{
  echo 'L\'attribut <strong>attribut</strong> n\'existe pas !<br />';
}

if (isset($obj->unAutreAttribut))
{
  echo 'L\'attribut <strong>unAutreAttribut</strong> existe !';
}
else
{
  echo 'L\'attribut <strong>unAutreAttribut</strong> n\'existe pas !';
}
http://localhost/phpmyadmin/tbl_structure.php?db=celine_rechatin_poo_exam&table=moto
j ai mis la capture d'ecran de la bdd dans upload.