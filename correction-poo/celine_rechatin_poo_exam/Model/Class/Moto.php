<?php
// Nos classes représentent les champs de notre BDD sous forme d'objet.
// Nous aurons en général 1 attribut pour chaque colonne
    class Moto{
        private $id;
        private $marque;
        private $modele;
        private $type;
        private $imageLink;

        public function __construct($marque, $modele, $type, $imageLink=null, $id=null){
            $this->id = $id;
            $this->type = $type;
            $this->marque = $marque;
            $this->modele = $modele;
            $this->imageLink = $imageLink;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function setMarque($marque){
            $this->marque = $marque;
        }

        public function getMarque(){
            return $this->marque;
        }



        public function getModele()
        {
            return $this->modele;
        }


        public function setModele($modele): void
        {
            $this->modele = $modele;
        }

        public function getType()
        {
            return $this->type;
        }


        public function setType($type): void
        {
            $this->type = $type;
        }


        public function getImageLink()
        {
            return $this->imageLink;
        }

        public function setImageLink($imageLink): void
        {
            $this->imageLink = $imageLink;
        }
    }
?>