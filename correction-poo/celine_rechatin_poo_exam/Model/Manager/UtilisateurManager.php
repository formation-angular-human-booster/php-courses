<?php



class UtilisateurManager extends DbConnexion
{


    public function login($username, $password)
    {
        $utilisateur = null;
        $req = $this->bdd->prepare("SELECT * FROM utilisateur WHERE username = :username");
        $req->execute([
            'username'=> $username
        ]);

        $resultat = $req->fetch();

        if($resultat){
            if(password_verify($password, $resultat['password'])){
                $utilisateur = new Utilisateur($resultat['username'], $resultat['password'], $resultat['id']);
            }
        }

        return $utilisateur;

    }



    public function findByUsername($username) {
        $utilisateur = null;
        $query = $this->bdd->prepare("SELECT * FROM utilisateur WHERE username = :username");
        $query->execute(['username'=> $username]);
        $utilisateurFromBdd = $query->fetch();

        if($utilisateurFromBdd){
            $utilisateur = new Utilisateur(
                $utilisateurFromBdd['username'],
                $utilisateurFromBdd['password'],
                $utilisateurFromBdd['id']);
        }


        return $utilisateur;
    }



    public function testExistUtilisateurByUsername($username){
        $user = $this->findByUsername($username);
        if($user){
            return true;
        } else {
            return  false;
        }
    }

    public function register($utilisateur){
        $query = $this->bdd->prepare("INSERT INTO utilisateur (username, password)VALUES (:username, :password) ");

        $query->execute([
            'password'=> password_hash($utilisateur->getPassword(), PASSWORD_DEFAULT),
            'username'=> $utilisateur->getUsername()
        ]);
    }

}
