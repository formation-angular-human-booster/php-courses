<?php


class MotoManager extends DbConnexion
{
    
    public function __construct()
    {

        parent::__construct();
    }

    
    public function getAll()
    {
        $motos = [];
        $query = $this->bdd->prepare("SELECT * FROM moto ORDER BY type;");
        $query->execute();
        $resultats = $query->fetchAll();

        foreach ($resultats as $resultat) {
            
            $motos[] = new Moto($resultat['type'], $resultat['marque'], $resultat['model'], $resultat['imageLink'], $resultat['id']);
        }

        return $motos;
    }

    public function getOne($id)
    {
        $moto = null;

        $query = $this->bdd->prepare("SELECT * FROM moto WHERE id = :id");
        $query->execute(['id' => $id]);

        $res = $query->fetch();

        if ($res) {
            $moto = new Moto($res['marque'], $res['model'], $res['type'], $res['imageLink'], $res['id']);
        }

        return $moto;
    }


    public function create(Moto $moto)
    {
        $query = $this->bdd->prepare("INSERT INTO moto (model, marque, type, imageLink) VALUES (:model, :marque, :type, :imageLink)");
        $query->execute([
            'model' => $moto->getModele(),
            'marque' => $moto->getMarque(),
            'type' => $moto->getType(),
            'imageLink' => $moto->getImageLink()
        ]);
    }

    public function delete(Moto $moto)
    {
        $query = $this->bdd->prepare("DELETE FROM moto WHERE id = :id");
        $query->execute(['id' => $moto->getId()]);
    }


}

?>

