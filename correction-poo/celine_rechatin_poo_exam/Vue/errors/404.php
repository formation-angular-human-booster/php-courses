<html>
<head>
<link rel="stylesheet" href="Public/css/404.scss">
</head>
<body>
<div class="box">
    <div class="box__ghost">
        <div class="symbol"></div>
        <div class="symbol"></div>
        <div class="symbol"></div>
        <div class="symbol"></div>
        <div class="symbol"></div>
        <div class="symbol"></div>

        <div class="box__ghost-container">
            <div class="box__ghost-eyes">
                <div class="box__eye-left"></div>
                <div class="box__eye-right"></div>
            </div>
            <div class="box__ghost-bottom">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="box__ghost-shadow"></div>
    </div>

    <div class="box__description">
        <div class="box__description-container">
            <div class="box__description-title">Whoops!</div>
             <div><?php if($message){echo($message);} ?></div>
        </div>

        <a href="javascript:history.back()" class="box__button">Revenir</a>

    </div>

</div>
    <script rel="script" src="Public/js/jquery.min.js"></script>
    <script rel="script" src="Public/js/404.js"></script>
</body>
</html>