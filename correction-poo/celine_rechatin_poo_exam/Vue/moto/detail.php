<html>
<head>
    <?php
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">


    <?php
    include 'Vue/parts/menu.php';
    ?>

    <h1>Détail de la moto <?php echo($moto->getType().' '.$moto->getMarque().' '.$moto->getModele().' '.$moto->getImageLink());?></h1>
    <h2>Cette moto possède l'id n° <?php echo($moto->getId());?></h2>

    <?php
     if(!is_null($moto->getImageLink())){
         echo("<img src='".$moto->getImageLink()."'>");
     }
    ?>
    <a href="index.php?controller=moto&action=list">Retour</a>
    <a href="index.php?controller=moto&action=delete&id=<?php echo($moto->getId());?>">supprimer</a>

</div>
</body>

<?php
include 'Vue/parts/global-scripts.php';
?>

</html>
