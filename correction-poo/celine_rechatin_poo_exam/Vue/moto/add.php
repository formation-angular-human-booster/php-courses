<html>
<head>
    <?php
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Ajout d'une moto</h1>


    <?php
    include 'Vue/parts/menu.php';
    ?>

    <a href="index.php?controller=moto&action=list">Retour</a>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="exampleInputEmail1">Marque</label>
            <input type="text"
                   value="<?php echo((isset($lastSaisie['marque'])) ? $lastSaisie['marque'] : '') ?>"
                   class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="marque" placeholder="Marque de la moto">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir la marque</small>
        </div>

        <div class="form-group">
            <label for="modele">Modèle</label>
            <input
                    value="<?php echo((isset($lastSaisie['modele'])) ? $lastSaisie['modele'] : '') ?>"
                    type="text" class="form-control" id="modele" aria-describedby="modeleHelp" name="modele" placeholder="Modèle de la moto">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir le modèle</small>
        </div>
        <div class="form-group">
            <label for="type">Type</label>
            <select name="type" class="form-select">
              <option selected>Choisir le type</option>
              <option id=anduro value="Anduro">Anduro</option>
              <option id=roadster value="Roadster">Roadster</option>
              <option id=custom value="Custom">Custom</option>
              <option id=sportive value="Sportive">Sportive</option>
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Image</label>
            <input type="file" class="form-control" id="image_link" aria-describedby="imageHelp" name="image_link">
            <small id="emailHelp" class="form-text text-muted">Veuillez ajouter une image</small>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php
        require'Vue/parts/form-error.php';
    ?>
</div>
</body>

<?php
include 'Vue/parts/global-scripts.php';
?>

</html>
