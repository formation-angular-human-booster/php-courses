<html>
<head>
    <?php
    // Inclusion du CSS global de l'application
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Affichage de toutes les motos</h1>

    <?php
    // Inclusion de mon menu
        include 'Vue/parts/menu.php';
    ?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Type</th>
            <th scope="col">Marque</th>
            <th scope="col">Modèle</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // Je parcours mon tableau de motos
        foreach ($motos as $moto){
            // Pour chacune des motos,
            // j'appelle les getteurs
            // pour afficher la valeur des attributs privés

            echo('<tr>
            <th scope="row">'.$moto->getId().'</th>
            <td>'.$moto->getType().'</td>
            <td>'.$moto->getMarque().'</td>
            <td>'.$moto->getModele().'</td>
         
            <td>

               <a href="index.php?controller=moto&action=detail&id='.$moto->getId().'">
                Voir en détail 
            </a>

                <a href="index.php?controller=moto&action=delete&id='.$moto->getId().'">
            Supprimer
                </a>
            </td>
        </tr>');
        }
        ?>

        </tbody>
    </table>
</div>
</body>

<?php

include 'Vue/parts/global-scripts.php';
?>

</html>
