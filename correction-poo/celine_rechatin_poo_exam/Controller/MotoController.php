<?php
    class MotoController {
        
        private $motoManager;

        
        public function __construct(){

            $this->motoManager = new MotoManager();


        }

        public function list(){
            $motos =[];
            
            $motos = $this->motoManager->getAll();

            require 'Vue/moto/list.php';
        }


        public function add(){

            $errors = [];
            $lastSaisie = [];
            
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $errors = $this->checkForm();
                $fileUrl = null;


                if(isset($_FILES['image_link'])){
                    $resultat = $this->uploadFile();
                    if($resultat['success']){
                        $fileUrl = $resultat['file_name'];
                    } else {
                        $errors[] = $resultat['errors'];
                    }
                }


                if(count($errors) == 0){
                  $moto = new Moto($_POST['marque'], $_POST['modele'], $_POST['type'], $fileUrl);

                  $this->motoManager->create($moto);

                  header('Location: index.php?controller=moto&action=list');
                  exit();
                }
            }

            require 'Vue/moto/add.php';
        }



        public function detail($id){

            $moto = $this->motoManager->getOne($id);


            if($moto != null){
                require 'Vue/moto/detail.php';

            } else {
                header('Location: index.php?controller=error&action=not-found&message=moto-not-found');
            }
        }

        public function delete($id){
            $moto = $this->motoManager->getOne($id);

            if($moto != null){
               $this->motoManager->delete($moto);
               header("Location: index.php?controller=moto&action=list");
            } else {
                header('Location: index.php?controller=error&action=not-found&message=moto-not-found');
            }


        }

        private function checkForm(){
            $errors = [];
            if(empty($_POST['marque'])){
                $errors[] = 'Veuillez saisir une marque';
            } else {
                $lastSaisie['marque'] = $_POST['marque'];
            }

            if(empty($_POST['modele'])){
                $errors[] = 'Veuillez saisir un modèle';
            } else {
                $lastSaisie['modele'] = $_POST['modele'];
            }
            if(empty($_POST['type'])){
                $errors[] = 'Veuillez saisir un type';
            } else {
                $lastSaisie['type'] = $_POST['type'];
            }

            return $errors;
        }

        public function uploadFile(){
            $errors = [];
            $uploadExtension = ['image/jpeg', 'image/png'];

            if($_FILES['image_link']['error'] != 0){
                $errors[] = 'erreur lors de l\'upload de l\'image';
            } 

            if(!in_array($_FILES['image_link']['type'], $uploadExtension)){
                $errors[] = 'Veuillez ajouter une image png ou jpeg';
            }

            if($_FILES['image_link']['size'] > 1000000){
                $errors[] = 'Fichier trop lourd';
            }

            if(count($errors) == 0){
                $name = uniqid().'.'.explode('/', $_FILES['image_link']['type'])[1];
                move_uploaded_file($_FILES['image_link']['tmp_name'], 'public/uploads/'.$name);
                return ['success'=> true, 'file_name'=> 'public/uploads/'.$name];
            } else {
                return ['success'=> false, 'errors'=> $errors];
            }
        }

    }
