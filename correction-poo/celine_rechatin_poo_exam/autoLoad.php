 <?php



 spl_autoload_register(function ($className) {

     $paths = array(
         'Model/Class/',
         'Controller/',
         'Model/Manager/',
     );

     // On parcourt chacun de ces dossiers
     foreach($paths as $path)
     {
         // On vérifie qu'il y a la classe en question dans ce dossier
         if(file_exists($path.$className . '.php'))
         {
             // On inclu notre classe si elle est présente
             require_once($path.$className . '.php');
             // on peut utiliser require tout cours voir elzero

             return;

         }
     }
 });
