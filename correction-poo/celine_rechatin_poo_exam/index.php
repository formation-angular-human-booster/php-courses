<?php
require 'autoLoad.php';

//session
if (!isset($_GET['controller']) || !isset($_GET['action'])) {
    header('Location: index.php?controller=security&action=login');
}

if ($_GET['controller'] == 'security') {
    $controller = new SecurityController();
    if ($_GET['action'] == 'login') {
        $controller->login();
    }

    if ($_GET['action'] == 'register') {
        $controller->register();
    }

    if ($_GET['action'] == 'logout') {
        $controller->logout();
    }
}


if ($_GET['controller'] == 'moto') {
    if (empty($_SESSION) || !$_SESSION['user']) {
        header('Location: index.php?controller=security&action=login');
    }

    $controller = new MotoController();



    if ($_GET['action'] == 'list') {
        $controller->list();
    }
    if ($_GET['action'] == 'add') {
        $controller->add();
    }

    if ($_GET['action'] == 'detail' && isset($_GET['id'])) {
        $controller->detail($_GET['id']);
    }

    if ($_GET['action'] == 'delete' && isset($_GET['id'])) {
        $controller->delete($_GET['id']);
    }

}
