<?php
$hostname = 'mysql';
$user = 'root';
$password = 'tiger';
$dbName = 'pagination';
try {
    $pdo = new PDO(
        'mysql:host='.$hostname.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);
    // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
    echo('Impossible de se connecter à la bdd');
    var_dump($e);
    die();
}