-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: toto
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `planets`
--

DROP TABLE IF EXISTS `planets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planets` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `name` varchar(250) DEFAULT NULL,
                         `status` varchar(250) DEFAULT NULL,
                         `terrain` varchar(250) DEFAULT NULL,
                         `allegiance` varchar(250) DEFAULT NULL,
                         `key_fact` varchar(250) DEFAULT NULL,
                         `image` varchar(250) DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planets`
--

LOCK TABLES `planets` WRITE;
INSERT INTO `planets` (`id`, `name`, `status`, `terrain`, `allegiance`, `key_fact`, `image`) VALUES (1,'Alderaan','Several of Alderaan\'s noble houses are battling for the throne in a brutal civil war.','Forests, Hills and Snow-Capped Mountains.','Independent','Following the Treaty of Coruscant, Alderaan seceded from the Galactic Republic.','alderaan.png');
INSERT INTO `planets` (`id`, `name`, `status`, `terrain`, `allegiance`, `key_fact`, `image`) VALUES (2,'Balmorra','Occupied by the Empire','War-torn Plains','Independent','The Sith Empire has taken a special interest in the manufacturing facilities on Balmorra, but a local resistance stands between them and full occupation.','balmorra.png');
UNLOCK TABLES;
