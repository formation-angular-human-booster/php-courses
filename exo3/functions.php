<?php
function countLetters($string, $letter) {
    $array = str_split( $string);
    $nbLetter = 0;
    foreach ($array as $item){
        if(strtoupper($item) === strtoupper($letter)) {
            $nbLetter +=1;
        }
    }
    return $nbLetter;
}
function reverseStringMethod1($string) {
    $str = str_split($string);
    $str = array_reverse($str);
    return implode('', $str);
}

function reverseStringMethod2($string) {
    $reverse = [];
    for ($i = strlen($string)-1; $i>=0; $i--) {
        $reverse[] = $string[$i];
    }
    return implode('', $reverse);
}

function reverseStringMethod3($string) {
    return strrev($string);
}
?>