<?php
class Session {
    public $attributs = [];

    public function __construct()
    {
        session_start();
        if($_SESSION && $_SESSION['exoSession']){
           $this->attributs = unserialize($_SESSION['exoSession'])->attributs;
        }
    }

    public function __isset($name)
    {
       return isset($this->attributs[$name]);
    }

    public function __unset($name)
    {
        unset($this->attributs[$name]);
    }

    public function __set($name, $value)
    {
        $this->attributs[$name] = $value;
    }

    public function __get($name)
    {

        return $this->attributs->$name;
    }

    public function save() {
        $_SESSION['exoSession'] = serialize($this);
    }

    public function logout() {
        session_destroy();
    }

    public function __sleep() {
        return ['attributs'];
    }

    public function displaySession(){
        var_dump(unserialize($_SESSION['exoSession']));
        var_dump($this->attributs);
    }
}