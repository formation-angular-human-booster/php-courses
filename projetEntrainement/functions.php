<?php
function reverseStringMethod1($string) {
    $str = str_split($string);
    $str = array_reverse($str);
    return implode('', $str);
}

function reverseStringMethod2($string) {
    $reverse = [];
    for ($i = strlen($string)-1; $i>=0; $i--) {
        $reverse[] = $string[$i];
    }
    return implode('', $reverse);
}

function reverseStringMethod3($string) {
    return strrev($string);
}

function countLetter($chaine, $searchLetter) {
    $nbLetter = 0;
    for($i=0; $i<strlen($chaine); $i++){
        if($chaine[$i] === $searchLetter){
            $nbLetter += 1;
        }
    }

    return $nbLetter;
}

// Solution
function countLetterSimple($chaine, $lettre){
    return substr_count($chaine, $lettre);
}

?>