<?php
 $adressesMail = [
    'aureliendelorme1@gmail.com',
    'smithcrank@gmail.com',
    '148547@supinfo.com',
    'aurelien.delorme@orange.fr',
    'test@yahoo.com',
    'bonjour@msn.com',
    'adelorme@humanbooster.com',
     "toto@toto.fr",
     "tata@toto.fr"
 ];

 function countEmail($adressesMail){
     $nbElement = count($adressesMail);


     // On cré un nouveau tableau de domaine
     $domains = [];

     // Pour chacune de nos adresse mail nous effectuerons ce traitement :
     foreach ($adressesMail as $mail){
         // Je réccupére le nom de domaine de mon adresse
         $arrayExploeded = explode('@',$mail);
         // Je vérifie que dans le nouveau tableau de la ligne 15 je n'ai pas une clé avec mon domaine
         if(!array_key_exists($arrayExploeded[1],$domains)) {
             // J'assigne 1 à la valeur de domaine['nomDeDomaine']
             $domains[$arrayExploeded[1]]['value'] = 1 ;

         } else {
             $domains[$arrayExploeded[1]]['value'] = $domains[$arrayExploeded[1]]['value'] + 1 ;

         }
         $domains[$arrayExploeded[1]]['pourcentage'] = $domains[$arrayExploeded[1]]['value'] / $nbElement * 100;
     }

// Utilisation de la fonction PHP
     return $domains;
 }
?>

<html>
<head>

</head>
<body>
<?php
    $domainFiltered = countEmail($adressesMail);

    foreach ($domainFiltered as $domaine=>$value){
        echo('Nom de domaine : '. $domaine . '<br>');
        echo('Nombre d\'affichage : '. $value['value']. '<br>');
        echo('Pourcentage d : '. $value['pourcentage']. '<br>');
    }
?>
</body>
</html>
