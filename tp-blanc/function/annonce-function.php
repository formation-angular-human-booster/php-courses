<?php
require_once 'db-connect.php';

    function getAllAnnonces(){
        $pdo = connectBdd();
        $query = $pdo->prepare("SELECT * FROM annonce ORDER BY id DESC");
        $query->execute();
        return $query->fetchAll();
    }

    function createAnnonce($titre, $contenu, $image){
        $pdo = connectBdd();

        $req = $pdo->prepare( 'INSERT INTO annonce
    (image_link, contenu, titre , nom_prenom_utilisateur)
    VALUES(:image_link, :contenu, :titre, :journaliste)');
        $req->execute([
            'image_link'=> $image,
            'contenu'=> $contenu,
            'titre'=> $titre,
            'journaliste'=> $_SESSION['nom'].' '.$_SESSION['prenom']
        ]);

    }
?>