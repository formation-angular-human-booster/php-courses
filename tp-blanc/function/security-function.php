<?php
require_once 'db-connect.php';

function checkAdmin(){
    if(empty($_SESSION) || empty($_SESSION['nom']) || empty($_SESSION['prenom'])){
        header('Location: login.php');
    }
}

function loginAttempt($username, $password){
    $pdo = connectBdd();
    $req = $pdo->prepare("SELECT * FROM utilisateur 
WHERE login = :login");
    $req->execute(
        [
            'login'=> $username
        ]);

    $user = $req->fetch();

    if(isset($user['password']) && password_verify($password, $user['password'])){
        return $user;
    } else {
        return false;
    }
}

function getAllUser(){

    $pdo = connectBdd();
    $req = $pdo->prepare("SELECT * FROM utilisateur");
    $req->execute();
    return $req->fetchAll();
}


function getUserByUsername($username){
    $pdo = connectBdd();
    $req = $pdo->prepare("SELECT * FROM utilisateur 
WHERE login = :login");
    $req->execute(
        [
            'login'=> $username
        ]);

    return $req->fetch();
}

function createUser($nom, $login, $prenom,$password){
    $pdo = connectBdd();

    $req = $pdo->prepare( 'INSERT INTO utilisateur(login, nom, prenom , password)
    VALUES(:login, :nom, :prenom, :password)');
    $req->execute([
        'login'=> $login,
        'nom'=> $nom,
        'prenom'=> $prenom,
        'password'=> $password
    ]);
}

?>