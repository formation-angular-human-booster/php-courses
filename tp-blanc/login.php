<?php
    require 'function/security-function.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $errors = [];

        if(empty($_POST['username'])){
            $errors[] = 'Veuillez inscrire un username';
        }

        if(empty($_POST['password'])){
            $errors[] = 'Veuillez saisir un mot de passe';
        }

        $user = loginAttempt($_POST['username'], $_POST['password']);

        if($user){
            $_SESSION['nom'] = $user['nom'];
            $_SESSION['prenom'] = $user['prenom'];

            header('Location: admin.php');
        } else {
            $errors[] = 'Les identifiants sont incorrects';
        }


    }
?>
<html>
<head>
    <?php
        include 'parts/global-stylesheets.php';
    ?>
    <link href="css/login.css" rel="stylesheet">
</head>
<body>
<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" method="post">
                        <h3 class="text-center text-info">Login</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="text" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group error">
                            <?php
                            if(isset($errors)){
                                foreach ($errors as $error){
                                    echo('<div class="error">'. $error.'</div>');
                                }
                            }

                            ?>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>