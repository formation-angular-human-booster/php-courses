<?php
    require 'function/security-function.php';
    checkAdmin();
?>

<html>
<head>
    <?php
        include 'parts/global-stylesheets.php'
    ?>
</head>
<body>

<?php
    include 'parts/admin/admin-menu.php';
?>

<?php
    include 'parts/global-scripts.php'
?>
</body>
</html>
