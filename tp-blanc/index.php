<?php
    require 'function/annonce-function.php';
?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<header>

    <!-- Jumbotron -->
    <div class="p-5 text-center bg-light">
        <h1 class="mb-3">La montagne</h1>
        <img src="images/lamontagne.png">
    </div>
    <!-- Jumbotron -->
</header>

<div class="container">
    <!--Ceci correspond à l'affichage d'une annonce en html-->
    <?php
        $annonces = getAllAnnonces();

        foreach ($annonces as $annonce){
            echo('    <div class="col-md-12 annonce mt-5 annonce">
        <h1 class="col-md-12 text-center">'.$annonce['titre'].'</h1>

        <div class="col-md-12 text-center">
            <img class="m-auto" src="'.$annonce['image_link'].'">
        </div>
        <div class="col-md-12 text-center">
            <span>'.$annonce['contenu'].'</span>
        </div>
        <div class="col-md-12 text-center">
            <span><u>'.$annonce['nom_prenom_utilisateur'].'</u></span>
        </div>
    </div>');
        }
    ?>
</div>
<?php
include 'parts/global-scripts.php'
?>
</body>
</html>