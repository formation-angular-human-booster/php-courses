<?php
require 'function/security-function.php';
require 'function/annonce-function.php';
checkAdmin();
$allowedExtension = ['image/jpeg', 'image/png'];
$filename = null;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $errors = [];
    if (empty($_POST['titre'])) {
        $errors[] = 'Veuillez saisir un titre';
    }

    if (empty($_POST['contenu'])) {
        $errors[] = 'Veuillez saisir un contenu';
    }


    if (isset($_FILES['annonce_picture']) && $_FILES['annonce_picture']['size'] != 0) {
        $image = $_FILES['annonce_picture'];
        if ($image['error'] != 0) {
            $errors[] = 'Erreur dans l\'envoi de l\'image';
        }

        if (!in_array($image['type'], $allowedExtension)) {
            $errors[] = 'Format du fichier incorrecte !';
        }

        if ($image['size'] > 100000) {
            $errors[] = 'Fichier trop volumineux';
        }

        if(count($errors) == 0){
            $uniqId = uniqid();
            $extension = explode('/',$image['type'])[1];
            $filename = 'uploads/'.$uniqId.'.'.$extension;
            move_uploaded_file($image['tmp_name'], $filename);
        }

    }

    if(count($errors) == 0){
        createAnnonce($_POST['titre'], $_POST['contenu'], $filename);
    }

}


?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>
<body>
<?php
include 'parts/admin/admin-menu.php';
?>

<div class="container">
    <h1>Administration des annonces</h1>


    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titre</th>
            <th scope="col">Contenu</th>
            <th scope="col">Image</th>
            <th scope="col">Journaliste</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $annonces = getAllAnnonces();

        foreach ($annonces as $annonce) {
            echo('<tr>
        <th scope="col">' . $annonce['id'] . '</th>
        <td scope="col">' . $annonce['titre'] . '</td>
         <td scope="col">' . $annonce['contenu'] . '</td>
         <td scope="col"><img class="mignature" src="' . $annonce['image_link'] . '"></td>
         <td scope="col">' . $annonce['nom_prenom_utilisateur'] . '</td>
    </tr>');
        }
        ?>
        </tbody>
    </table>

    <form class="dotted-form" method="post" enctype="multipart/form-data">
        <h2 class="text-center">Ajouter une annonce</h2>
        <div class="form-group">
            <label for="exampleInputEmail1">Titre</label>
            <input type="text" class="form-control" id="titre"
                   placeholder="Entrez un titre" name="titre">
        </div>
        <div class="form-group">
            <label for="nomInput">Contenu</label>
            <input type="text" name="contenu" class="form-control" id="contenu"
                   placeholder="Veuillez saisir un contenu">
        </div>

        <div class="form-group">
            <label for="nomInput">Image</label>
            <input type="file" name="annonce_picture" class="form-control" id="contenu"
                   placeholder="Veuillez saisir un contenu">
        </div>


        <div class="form-group error">
            <?php
            if (isset($errors)) {
                foreach ($errors as $error) {
                    echo('<div class="error">' . $error . '</div>');
                }
            }

            ?>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</div>
<?php
include 'parts/global-scripts.php'
?>
</body>
</html>
