<?php
require 'function/security-function.php';



checkAdmin();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $errors = [];

    if(empty($_POST['login'])){
        $errors[] = 'Veuillez saisir un nom d\'utilisateur';
    }

    if(empty($_POST['nom'])){
        $errors[] = 'Veuillez saisir le nom du journaliste';
    }

    if(empty($_POST['prenom'])){
        $errors[] = 'Veuillez saisir un prénom';
    }

    if(empty($_POST['password'])){
        $errors[] = 'Veuillez saisir un mot de passe';
    }

    if(empty($_POST['confirm-password'])){
        $errors[] = 'Veuillez confirmer votre mot de passe';
    }

    if($_POST['password'] != $_POST['confirm-password']){
        $errors[] = 'Les mots de passes ne sont pas identiques';
    }

    $user = getUserByUsername($_POST['login']);

    if($user != false){
        $errors[] = 'Impossible il existe déjà !';
    }
    if(count($errors) == 0){
        createUser($_POST['nom'], $_POST['login'], $_POST['prenom'], password_hash($_POST['password'], PASSWORD_DEFAULT));
    }
}


?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>
<body>
<?php
include 'parts/admin/admin-menu.php';
?>

<div class="container">
<h1>Administration des utilisateurs</h1>



<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Login</th>
        <th scope="col">Nom</th>
        <th scope="col">Prenom</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $users = getAllUser();

    foreach ($users as $user){
        echo('<tr>
        <th scope="col">'.$user['id'].'</th>
        <td scope="col">'.$user['login'].'</td>
         <td scope="col">'.$user['nom'].'</td>
         <td scope="col">'.$user['prenom'].'</td>
    </tr>');
    }
    ?>
    </tbody>
</table>

    <form class="dotted-form" method="post">
        <h2 class="text-center">Ajouter un utilisateur</h2>
        <div class="form-group">
            <label for="exampleInputEmail1">Login</label>
            <input type="text" class="form-control" id="loginInput"
                  placeholder="Entrez un login" name="login">
        </div>
        <div class="form-group">
            <label for="nomInput">Nom</label>
            <input type="text" name="nom" class="form-control" id="nomInput" placeholder="Nom de famille">
        </div>

        <div class="form-group">
            <label for="nomInput">Prénom</label>
            <input type="text" name="prenom" class="form-control" id="prenomInput" placeholder="Prénom">
        </div>

        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Password">
        </div>

        <div class="form-group">
            <label for="confirmPasswordInput">Mot de passe</label>
            <input type="password" name="confirm-password" class="form-control" id="confirmPasswordInput" placeholder="Confirmez votre MDP">
        </div>

        <div class="form-group error">
            <?php
            if(isset($errors)){
                foreach ($errors as $error){
                    echo('<div class="error">'. $error.'</div>');
                }
            }

            ?>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

<?php
include 'parts/global-scripts.php'
?>
</div>
</body>
</html>
