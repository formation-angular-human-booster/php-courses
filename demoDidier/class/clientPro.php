<?php
class ClientPro extends Client implements ClientProInterface {
    private $customAttributes;
    private $siegeSocial;

    public function __construct($nom, $prenom, $siegeSocial)
    {
        echo('Je construit un client pro');
        $this->siegeSocial = $siegeSocial;
        parent::__construct($nom, $prenom);
        $this->customAttributes = [];
    }

    public function __set($name, $valeur){
        //  echo($name. " n'existe pas ! impossible de lui attribuer ". $valeur);
        $this->customAttributes[$name] = $valeur;
    }

    public function __get($name){
        return $this->customAttributes[$name];
    }
    public function getSiegeSocial()
    {
        // TODO: Implement getSiegeSocial() method.
    }

    public function setSiegeSocial($siegeSocial)
    {
        // TODO: Implement setSiegeSocial() method.
    }

    public function __toString()
    {
       return $this->prenom. ' localisé à ' . $this->siegeSocial;
    }
}