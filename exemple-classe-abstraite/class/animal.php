<?php
    abstract class Animal implements AnimalInterface {
        private $nom;

        public function __construct()
        {
            echo('Nouvel Animal ! ');
        }

        public function getNom() {
            return $this->nom;
        }

        public function setNom($nom) {
            $this->nom = $nom;
        }

        public function manger($nouriture){
            echo('Je mange '. $nouriture);
        }
    }
?>