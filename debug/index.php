<?php
include('class/animal.php');

$animal = new Animal('Renard', 10);
$animal2 = new Animal('Tortue', 6);

echo ('Mon animal est : ' . Animal::$Type[1] . '<br><br>');

echo ('Je construis un objet animal qui à pour nom :' . $animal->getName() . ' et pour age ' . $animal->getAge() . ' ans<br><br>');

echo ('Mon second animal est : ' . Animal::$Type[0] . '<br><br>');

echo ('Je construis un second objet animal qui à pour nom :' . $animal2->getName() . ' et pour age ' . $animal2->getAge() . ' ans<br><br>');