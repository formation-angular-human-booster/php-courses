<?php
class Animal
{

    private $age;
    public $nom;

    public static $Type = ['Herbivore', 'Carnivore'];


    public function __construct($nom, $age)
    {
        $this->nom = $nom;
        $this->age = $age;
    }

    public function __destruct()
    {
        echo ("Animal détruit !");
    }


    public function getName()
    {
        return $this->nom;
    }


    public function getAge()
    {
        return $this->age;
    }
}