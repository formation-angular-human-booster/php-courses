<?php
class Session{

    private $attributs = [];


    public function __set($key, $valeur){
        $this->attributs[$key] = $valeur;
    }

    public function __get($key){
        return $this->attributs[$key];

    }

    public function __isset($key){
        return isset($this->attributs[$key]);
    }

    public function __unset($key){
        unset($this->attributs[$key]);
    }
    

    public function getAttributs(){
        return $this->attributs;
    }
}
?>