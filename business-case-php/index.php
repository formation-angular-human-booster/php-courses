<html>
<head>
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
</head>
<body>
<?php

$products = [
    [
        'image' => 'croquettes.jpg',
        'nom' => 'Croquettes pour chien',
        'description'=> 'Pour tous types de chiens',
        'prix'=> 45.49
    ],
    [
        'image' => 'panier.jpg',
        'nom' => 'Pannier pour chien',
        'description'=> 'Grand pannier',
        'prix'=> 40.99
    ],
    [
        'nom' => 'Un produit sans image',
        'description'=> 'Un produit sans image',
        'prix'=> 10
    ],

]

?>
<?php include 'parts/menu.php' ?>

<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
    <div style="height: 300px;" class="carousel-inner">
        <?php
        foreach ($products as $key=>$product){
            ?>
            <div class="carousel-item <?php echo($key === 0 ? 'active': ''); ?>">
                <?php
                    if(array_key_exists($product['image'], $product)){
                        ?>
                        <img style="height: 300px; width: auto;" src="public/images/<?php echo($product['image']);?>"
                             class="d-block w-100" alt="...">
                        <?php
                    } else {
                        ?>
                        <img style="height: 300px; width: auto;" src="public/images/no-picture.png"
                             class="d-block w-100" alt="...">
                <?php
                    }
                ?>

            </div>
        <?php
        }
        ?>

    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>


<div class="row">
<?php
        foreach ($products as $product){

?>
            <div class="card" style="width: 18rem;">
                <?php
                if(array_key_exists('image', $product)){
                    ?>
                    <img style="height: 300px; width: auto;" src="public/images/<?php echo($product['image']);?>"
                         class="d-block w-100" alt="...">
                    <?php
                } else {
                    ?>
                    <img style="height: 300px; width: auto;" src="public/images/no-picture.png"
                         class="d-block w-100" alt="...">
                    <?php
                }


                ?>

                 <div class="card-body">
                    <h5 class="card-title"><?php echo($product['nom']) ?></h5>
                    <p class="card-text"><?php echo($product['description']) ?></p>
                    <a href="article.php" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <?php
        }
?>
</div>
<?php
    include 'parts/footer.php';
?>
    <script type="text/javascript" src="public/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/js/bootstrap.bundle.min.js"></script>
</body>