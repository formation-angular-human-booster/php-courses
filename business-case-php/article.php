<html>
<head>
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
</head>
<body>

<?php
 $article =  [
     'image' => 'croquettes.jpg',
     'nom' => 'Croquettes pour chien',
     'description'=> 'Pour tous types de chiens',
     'prix'=> 45.49
 ];
?>

<?php include 'parts/menu.php' ?>


<h1><?php echo($article['nom']);?></h1>
<div class="row">
<img src="public/images/<?php echo($article['image']);?>">
</div>

<div class="row">
    <div class="col-md-12"><?php echo($article['description']) ?></div></div>
<a href="index.php"><button class="btn btn-success">Retour !</button></a>
<?php
include 'parts/footer.php';
?>

<script type="text/javascript" src="public/js/bootstrap.min.js"
</body>