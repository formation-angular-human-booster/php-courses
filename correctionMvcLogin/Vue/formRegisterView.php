<html>
<head>
    <meta charset="utf-8">
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="assets/style.css" media="screen" type="text/css" />
</head>
<body>
<div id="container">
    <!-- zone de connexion -->

    <form action="index.php?controller=user&action=registerUser" method="POST">
        <h1>S'enregistrer</h1>

        <label><b>Nom de l'utilisateur</b></label>
        <input type="text" placeholder="Entrer le nom d'utilisateur" name="nom" required>

        <label><b>Prénom de l'utilisateur</b></label>
        <input type="text" placeholder="Entrer le prénom d'utilisateur" name="prenom" required>

        <label><b>Mot de passe de l'utilisateur</b></label>
        <input type="password" placeholder="Entrer le mot de passe" name="password" required>

        <input type="submit" id='submit' value='LOGIN' >
    </form>
    <a href="/correctionMvcLogin/?controller=user&action=displayLogin">Revenir au login</a>
</div>
</body>
</html>