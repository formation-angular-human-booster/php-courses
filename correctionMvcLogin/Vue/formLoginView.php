<html>
<head>
    <meta charset="utf-8">
    <!-- importer le fichier de style -->
    <link rel="stylesheet" href="assets/style.css" media="screen" type="text/css" />
</head>
<body>
<div id="container">
    <!-- zone de connexion -->

    <form action="index.php?controller=user&action=logUser" method="POST">
        <h1>Connexion</h1>

        <label><b>Nom d'utilisateur</b></label>
        <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

        <label><b>Mot de passe</b></label>
        <input type="password" placeholder="Entrer le mot de passe" name="password" required>

        <input type="submit" id='submit' value='LOGIN' >
    </form>

    <?php
        if(isset($_GET['error'])){
            echo($_GET['error']);
        }
    ?>
    <br>
    <a href="/correctionMvcLogin/?controller=user&action=register">S'enregistrer</a>
</div>
</body>
</html>