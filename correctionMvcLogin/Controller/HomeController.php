<?php
class HomeController {

    public function __construct()
    {
        if(empty($_SESSION['utilisateur'])) {
            header('Location: http://localhost/correctionMvcLogin/index.php?controller=user&action=displayLogin');
        }
    }

    public function displayDashboard() {
        $utilisateur = $_SESSION['utilisateur'];
        require 'Vue/dashboardView.php';
    }
}