<?php
class UserController {
    public function affichageFormulaireLogin(){
        require 'Vue/formLoginView.php';
    }

    public function affichageFormulaireRegister() {
        require 'Vue/formRegisterView.php';
    }

    public function registerNewUser() {
        $user = new UtilisateurModel(null, $_POST['nom'], $_POST['prenom'], $_POST['password']);
        $userManager = new UtilisateurManager();
        $userManager->insert($user);
        header('Location: http://localhost/correctionMvcLogin/index.php?controller=user&action=displayLogin');
    }

    public function logUser() {
        $userName = $_POST['username'];
        $password = $_POST['password'];
        $userManager = new UtilisateurManager();
        $testLogin = $userManager->testLogin($userName, $password);

        if($testLogin['error'] == null){

            $_SESSION['utilisateur'] = $testLogin['utilisateur']->getNom();

            header('Location: http://localhost/correctionMvcLogin/index.php?controller=home&action=displayHome');

            //$_GET['controller'] === 'home' && $_GET['action'] === 'displayHome'
        } else {
            header('Location: http://localhost/correctionMvcLogin/index.php?controller=user&action=displayLogin&error='.$testLogin['error']);
        }
    }

    public function logout() {
        session_destroy();
        header('Location: http://localhost/correctionMvcLogin/index.php?controller=user&action=displayLogin');
    }
}