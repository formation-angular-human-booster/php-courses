<?php
    session_start();
    require 'include.php';
    if($_GET['controller'] === 'user' && $_GET['action'] === 'displayLogin'){
        $userController = new UserController();
        $userController->affichageFormulaireLogin();
    } elseif ($_GET['controller'] === 'user' && $_GET['action'] === 'register') {
        $userController = new UserController();
        $userController->affichageFormulaireRegister();
    } elseif ($_GET['controller'] === 'user' && $_GET['action'] === 'registerUser'){
        $userController = new UserController();
        $userController->registerNewUser();
    } else if ($_GET['controller'] === 'user' && $_GET['action'] === 'logUser'){
        $userController = new UserController();
        $userController->logUser();
    } else if ($_GET['controller'] === 'user' && $_GET['action'] === 'logout') {
        $userController = new UserController();
        $userController->logout();
    }
    else if ($_GET['controller'] === 'home' && $_GET['action'] === 'displayHome') {
        $homeController = new HomeController();
        $homeController->displayDashboard();
    }
    else {
        throw new Exception('Page introuvable',404);
    }
?>