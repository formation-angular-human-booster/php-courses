<?php
 class UtilisateurManager extends DbManager {
     function __construct()
     {
         parent::__construct();
     }

     function getUserByEmailAndPassword() {
         // Todo
     }

     public function insert(UtilisateurModel $utilisateur)
     {
         $nom = $utilisateur->getNom();
         $prenom = $utilisateur->getPrenom();
         $password = $utilisateur->getPassword();
         $requete = $this->bdd->prepare("INSERT INTO utilisateur (nom, prenom, password) VALUES (?,?,?)");
         $requete->bindParam(1, $nom);
         $requete->bindParam(2, $prenom);
         $requete->bindParam(3, $password);
         $requete->execute();
         $utilisateur->setId($this->bdd->lastInsertId());
     }

     public function testLogin($userName, $password)
     {
         $requete = $this->bdd->prepare("SELECT * FROM utilisateur WHERE  nom = ? AND password = ?");
         $requete->bindParam(1, $userName);
         $requete->bindParam(2, $password);
         $requete->execute();
         $res = $requete->fetch();

         $erreur = null;
         $utilisateur = null;

         if($res === false) {
             $erreur = 'UtilisateurModel inconnu';
         } else {
             $utilisateur = new UtilisateurModel($res['id'], $res['nom'], $res['prenom'], $res['password']);
         }


         return ['error'=> $erreur, 'utilisateur'=> $utilisateur];
     }
 }