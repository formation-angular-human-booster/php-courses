<?php
 class Utilisateur {
     private $id;
     private $nom;
     private $prenom;
     private $password;

     public function __construct($id, $nom, $prenom, $password)
     {
         $this->id = $id;
         $this->nom = $nom;
         $this->prenom = $prenom;
         $this->password = $password;
     }


     /**
      * @return mixed
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param mixed $id
      */
     public function setId($id)
     {
         $this->id = $id;
     }

     /**
      * @return mixed
      */
     public function getNom()
     {
         return $this->nom;
     }

     /**
      * @param mixed $nom
      */
     public function setNom($nom)
     {
         $this->nom = $nom;
     }

     /**
      * @return mixed
      */
     public function getPrenom()
     {
         return $this->prenom;
     }

     /**
      * @param mixed $prenom
      */
     public function setPrenom($prenom)
     {
         $this->prenom = $prenom;
     }

     /**
      * @return mixed
      */
     public function getPassword()
     {
         return $this->password;
     }

     /**
      * @param mixed $password
      */
     public function setPassword($password)
     {
         $this->password = $password;
     }


 }