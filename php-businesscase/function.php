<?php

// Cette fonction vérifie si un utilisateur est connecté ou non
// Si il n'est pas connecté, elle redirige l'utilisateur vers le login
function checkConnexion(){
    // Si je n'ai pas de session ou que ma session ne contient l'attribut username
    // Je renvoie l'utilisateur vers la page de login

    if(!isset($_SESSION) || !isset($_SESSION['username'])){
        header('Location: login.php');
    }
}

function getOneUser($id, $pdo){
    $res = $pdo->prepare("SELECT * FROM utilisateur WHERE id = :id");
    $res->execute(['id' => $id]);
    $user = $res->fetch();

    return $user;
}

// Cette fonction vérifie si un utilisateur est connecté.
// Si c'est le cas elle le redirige vers le dashboard
// Elle sera notament utilisé sur le login pour qu'un utilisateur connecté puisse se re logg
function redirectConnectedUser(){
    if(isset($_SESSION) && isset($_SESSION['username'])){
        header('Location: dashboard.php');
    }
}

// Fonction en charge affiche soit le bouton ajouter au favori si on
// a pas encore le véhicule en favori
// Si on l'a déjà elle affichera supprimer des favoris
// On passe dans notre bouton la variable $redirect qui permettra
// de savoir si l'on souhaite rediriger vers le dashboard ou les favoris
// exemple :
// favori-add.php?id=1&redirect=dashboard redirigera au final vers dashboard.php
function displayFavoriButton($id, $redirect){
    if(isset($_COOKIE['favori'])){
        $cookieArray = explode(',', $_COOKIE['favori']);
        if(in_array($id, $cookieArray)){
            return '  <a href="favori-remove.php?id='.$id.'&redirect='.$redirect.'" class="btn btn-primary">Supprimer des favori</a>';

        } else {
            return '  <a href="favori-add.php?id='.$id.'&redirect='.$redirect.'" class="btn btn-primary">Ajouter au favori</a>';
        }

    } else {
        return '  <a href="favori-add.php?id='.$id.'&redirect='.$redirect.'" class="btn btn-primary">Ajouter au favori</a>';
    }

}

// Cette fonction est en charge de l'affichage d'un card
// On cré cette fonction pour ne pas avoir à dupliquer le code d'affichage des card
function displayCarCard($voiture, $redirect){
    // Cette fonction affichera le card. Elle appellera une autre fonction pour l'affichage du bouton
    // ajouter / supprimer des favoris
    echo('<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="images/'.$voiture['image'].'" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title">'.strtoupper($voiture['marque']).' '.$voiture['model'].'</h5>
        <p class="card-text">'.$voiture['description'].'</p>
        
      '.displayFavoriButton($voiture['identifiant'], $redirect).'
    </div>
</div>');
}

function getAllVoiture(){
    // Fonction qui retourne toutes nos voitures
    // Elle est composé d'un tableau numéroté
    // Chaque élément du tableau correspond à une voiture et est un tableau
    // clé => valeur
    return [
        [
            'identifiant'=> 1,
            'model'=> '4L',
            'marque'=> 'renault',
            'description'=> 'Super voiture qui a fait de la route !',
            'carburant' => 'Essence',
            'image' => '4l.jpg'
        ],
        [
            'identifiant'=> 2,
            'model'=> '308',
            'marque'=> 'Peugeot',
            'description'=> 'Super vieille voiture !!',
            'carburant' => 'diesel',
            'image' => '308.jpg'
        ],
        [
            'identifiant'=> 3,
            'model'=> 'Coccinelle',
            'marque'=> 'Volkswagen',
            'description'=> 'Livrée avec la roue de secours sur le toit !!',
            'carburant' => 'essence',
            'image' => 'coccinelle.jpg'
        ]
    ];
}