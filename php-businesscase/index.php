<?php
// nous sommes sur l'url principale de notre application (localhost/)

// On inclu nos fonctions pour pouvoir les réutiliser
require 'function.php';

// J'appelle la fonction contenue dans le fichier function.php
// checkConnexion
checkConnexion();

// J'appelle la fonction contenue dans le fichier function.php
// redirectConnextedUser
redirectConnectedUser();

?>