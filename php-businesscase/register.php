<html>
<head>
    <?php
    require 'function.php';
    redirectConnectedUser();
    // Importe le fichier global-stylesheets.php contenu dans le dossier parts
    // Les css qui sont utilisés par TOUTES les vues de notre application
    // Ceci évitera la duplication de code.
    include 'parts/global-stylesheets.php';
    ?>

    <!--
    Ici c'est un CSS spécifique à la page de login / register
    Ce css ne sera donc pas dans le fichier contenu dans parts/global-stylesheets.php
    -->
    <link rel="stylesheet" href="css/login.css">
</head>

<body>

<div id="login">
    <h3 class="text-center text-white pt-5">Register form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <!--
                    Ici : Mon formulaire sera envoyé au fichier situé manager/register-manager.php en utilisant la mèthode POST
                    Les données seront donc envoyés dans le corps de la requête

                    La propriete html enctype="multipart/form-data" permet à notre navigateur
                    d'autoriser l'upload d'image
                    -->
                    <form id="login-form" class="form" action="manager/register-manager.php" method="post" enctype="multipart/form-data">
                        <h3 class="text-center text-info">Register</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>

                        <!--
                        Ajout d'un input de type password pour que l'utilisateur ne se trompe pas et
                        réécrive son mot de passe
                        -->
                        <div class="form-group">
                            <label for="password" class="text-info">Confirm password:</label><br>
                            <input type="password" name="confirm-password" id="password" class="form-control">
                        </div>

                        <!--
                        Input de type file pour proposer à l'utilisateur d'ajouter sa photo
                        -->
                        <div class="form-group">
                            <label for="password" class="text-info">Avatar :</label><br>
                            <input type="file" name="profile_picture" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-info btn-md mt-3">
                        </div>

                        <!--
                        L'utilisateur à déjà un compte on lui met un bouton pour retourner
                        vers la page de login
                        -->
                        <a href="login.php">J'ai déjà un compte</a>
                    </form>


                            <?php
                            // Le traitement des erreurs en PHP
                            // Ces erreurs sont envoyés via les paramètres get de l'URL
                            if(isset($_GET['error'])) {

                                echo(' <div class="error text-center">');

                                // Je vérifie la valeur de mon erreur pour afficher le bon message
                                switch ($_GET['error']) {
                                    case 'no-username':
                                        echo('Tu n\'as pas saisie de nom d\'utilisateur');
                                        break;
                                    case 'no-password':
                                        echo('Tu n\'as pas saisie de mot de passe');
                                        break;
                                    case 'no-confirm':
                                        echo('Veuillez confirmer votre mot de passe');
                                        break;
                                    case 'not-same':
                                        echo('Les identifiants sont pas identiques !');
                                        break;
                                    case 'error-upload':
                                        echo('Erreur lors de l\'upload de l\'image');
                                        break;
                                    case 'bad-file':
                                        echo('Veuillez ajouter une image jpeg ou png');
                                        break;
                                    case 'large-file':
                                        echo('Veuillez ajouter une image moins lourde');
                                        break;
                                    case 'already-exist':
                                        echo('Veuillez choisir un autre username. Celui la est déjà pris');
                                        break;
                                }
                            }
                            echo('</div>');
                            ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
// Importe le fichier global-scripts.php contenu dans le dossier parts
// Les JS qui sont utilisés par TOUTES les vues de notre application
// Ceci évitera la duplication de code.
require 'parts/global-scripts.php';
?>

</body>
</html>