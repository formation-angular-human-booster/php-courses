<html>
<head>
    <?php
    // J'inclu mon fichier de fonction qui contien les différentes fonction que j'ai développé
    require 'function.php';
    // J'appelle la fonction check connexion contenu dans ce fichier pour rediriger
    // un utilisateur qui n'est pas connecté
    checkConnexion();
    // Importe le fichier global-stylesheets.php contenu dans le dossier parts
    // Les css qui sont utilisés par TOUTES les vues de notre application
    // Ceci évitera la duplication de code.
    include 'parts/global-stylesheets.php';
    ?>
</head>

<body>

<?php
// inclu le menu commun à notre application. Cela évite de dupliquer du code source
include 'parts/menu.php';
?>
<div class="row">
<?php
// On appelle la fonction getAllVoiture contenu dans le fichier function.php
// Cette fonction retourne notre tableau de voiture
$voitures = getAllVoiture();
if(isset($_COOKIE['favori']) && !empty($_COOKIE['favori'])){
    $voiturePrefereId = explode(',', $_COOKIE['favori']);
    foreach ($voiturePrefereId as $voitureId){
       foreach ($voitures as $voiture){
           if($voitureId == $voiture['identifiant']){
               displayCarCard($voiture, 'favori');
           }
       }
    }

    die();
} else {
    echo('<span class="col-md-12 error mt-5 text-center">Aucun favori !</span>');
}
?>
</div>
<?php
// Importe le fichier global-scripts.php contenu dans le dossier parts
// Les JS qui sont utilisés par TOUTES les vues de notre application
// Ceci évitera la duplication de code.
require 'parts/global-scripts.php';
?>
</body>
</html>