<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>
<?php
require 'db-connexion.php';
require 'function.php';

$user = getOneUser($_GET['id'], $pdo)
?>
<a href="pdo.php">Retourner au tableau</a>
<div class="text-center">
    <h1>Profil de <?php echo($user['username']); ?></h1>
    <img class="max-500" src="images/<?php echo($user['image_link']); ?>">
</div>
</body>
</html>


