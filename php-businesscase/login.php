<html>
<head>
    <?php
    require 'function.php';
    redirectConnectedUser();
    // Importe le fichier global-stylesheets.php contenu dans le dossier parts
    // Les css qui sont utilisés par TOUTES les vues de notre application
    // Ceci évitera la duplication de code.
    include 'parts/global-stylesheets.php';
    ?>

    <!--
    Ici c'est un CSS spécifique à la page de login
    Ce css ne sera donc pas dans le fichier contenu dans parts/global-stylesheets.php
    -->
    <link rel="stylesheet" href="css/login.css">
</head>

<body>

<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <!--
                    Ici : Mon formulaire sera envoyé au fichier situé manager/login-manager.php en utilisant la mèthode POST
                    Les données seront donc envoyés dans le corps de la requête
                    -->
                    <form id="login-form" class="form" action="manager/login-manager.php" method="post">
                        <h3 class="text-center text-info">Login</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-info btn-md mt-3">
                        </div>

                        <a href="register.php">Créer un compte</a>
                    </form>

                    <?php
                        // On vérifie ici si nous avons une erreur dans notre url
                        // via le paramètre get error
                        // Si on en a une, on affichera une div avec la classe error
                        if(isset($_GET['error'])){
                    ?>
                    <div class="error text-center">
                        <?php
                        // Je vérifie la valeur de mon erreur pour afficher le bon message
                            switch ($_GET['error']){
                                case 'no-saisie':
                                    echo('Tu n\'as rien saisie !');
                                    break;
                                case 'no-username':
                                    echo('Veuillez saisir votre nom d\'utilisateur');
                                    break;
                                case 'no-password':
                                    echo('Veuillez saisir un mot de passe !');
                                    break;
                                case 'bad-credentials':
                                    echo('Les identifiants sont mauvais !');
                                    break;
                                default:
                                    echo('Erreur inconnu désolé mais tu ne passes pas');
                                    break;
                            }
                        ?>
                    </div>

                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
// Importe le fichier global-scripts.php contenu dans le dossier parts
// Les JS qui sont utilisés par TOUTES les vues de notre application
// Ceci évitera la duplication de code.
require 'parts/global-scripts.php';
?>

</body>
</html>