<html>
<head>
    <?php
    // J'inclu mon fichier de fonction qui contien les différentes fonction que j'ai développé
    require 'function.php';
    // J'appelle la fonction check connexion contenu dans ce fichier pour rediriger
    // un utilisateur qui n'est pas connecté
    checkConnexion();
    // Importe le fichier global-stylesheets.php contenu dans le dossier parts
    // Les css qui sont utilisés par TOUTES les vues de notre application
    // Ceci évitera la duplication de code.
    include 'parts/global-stylesheets.php';
    ?>
</head>

<body>
<?php
// inclu le menu commun à notre application.
// Cela évite de dupliquer du code source
include 'parts/menu.php';
?>
<h1>Bienvenue M. Lodevie</h1>

<?php
// Je déclare un tableau numéroté de voiture
// Chaque élément du tableau sera un autre tableau clé => valeur
// Chaque sous tableau contiendra : Un modèle, une marque, une description, un carburant
// et une image
$voitures = getAllVoiture();
?>

<div class="row">
<?php
// Je parcours toutes les voitures récupérées à la ligne 29
foreach ($voitures as $voiture){
    // Pour chaque voiture j'affiche son card
    // via la fonction displayCarCard du fichier function.php
    // Je lui envoie aussi vers quelle page je souhaite rediriger mon
    // utilisateur une fois que l'action sera terminée
    displayCarCard($voiture, 'dashboard');
}
?>
</div>

<?php
// Importe le fichier global-scripts.php contenu dans le dossier parts
// Les JS qui sont utilisés par TOUTES les vues de notre application
// Ceci évitera la duplication de code.
require 'parts/global-scripts.php';
?>
</body>
</html>