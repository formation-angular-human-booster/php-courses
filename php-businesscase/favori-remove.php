<?php
// Vérifie que l'on a déjà des favori
if(isset($_COOKIE['favori'])){

    // Si c'est le cas, il transforme le string stocké dans
    // la variable super globale $_COOKIE['favori'] en tableau

    $voiturePrefere = explode(',', $_COOKIE['favori']);

} else {
    // Sinon je n'ai pas encore de favori. Je déclare donc une variable
    // qui est un tableau vide
    $voiturePrefere = [];
}

// Je réccupére l'id de l'élément que je souhaite supprimer via
// la super globale $_GET
$element = $_GET['id'];

// Je supprime l'id dans mon tableau de favori que j'ai passé en GET
unset($voiturePrefere[array_search($element, $voiturePrefere)]);

// Je retransforme mon tableau en chaine de caractère
// On est obligé puisque l'on ne peut pas stocker de tableau
// dans les cookies mais seulement des chaines de caractères
$voiturePrefere = implode(",", $voiturePrefere);

// Je met à jour mon cookie
setcookie("favori", $voiturePrefere);

// Je redirige l'utilisateur depuis son lieu d'arriver
// Nous utilisons le paramètre de la super globale $_GET afin de savoir
// d'ou il vient
// exemple : favori-add.php?id=1&redirect=dashboard
// redirigera l'utilisateur vers dashboard.php
header("Location: ".$_GET['redirect'].".php");

?>