<?php
require 'db-connexion.php';
require 'function.php';

$user = getOneUser($_GET['id'], $pdo);


?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>

<body>
<form action="manager/update-manager.php?id=<?php echo($user['id']);?>"
      enctype="multipart/form-data" method="post">
    <div class="form-group">
    <label>Username</label>
    <input type="text" value="<?php echo($user['username']);?>" name="username" placeholder="username">
    </div>

    <div class="form-group">
    <label>Password</label>
    <input type="password" value="<?php echo($user['password']);?>" name="password" placeholder="password">
    </div>

    <div class="form-group">
    <label>Confirmation du password</label>
    <input type="password" value="<?php echo($user['password']);?>" name="confirm-password" placeholder="password">
    </div>

    <div class="form-group text-center">
        <label>Image (uploadez en une nouvelle si elle ne vous convient pas)</label>
        <img src="images/<?php echo($user['image_link']);?>">

    <input type="file" name="profile_picture" class="form-control">
    </div>

    <?php
    // Le traitement des erreurs en PHP
    // Ces erreurs sont envoyés via les paramètres get de l'URL
    if(isset($_GET['error'])) {

        echo(' <div class="error text-center">');

        // Je vérifie la valeur de mon erreur pour afficher le bon message
        switch ($_GET['error']) {
            case 'no-username':
                echo('Tu n\'as pas saisie de nom d\'utilisateur');
                break;
            case 'no-password':
                echo('Tu n\'as pas saisie de mot de passe');
                break;
            case 'no-confirm':
                echo('Veuillez confirmer votre mot de passe');
                break;
            case 'not-same':
                echo('Les identifiants sont pas identiques !');
                break;
            case 'error-upload':
                echo('Erreur lors de l\'upload de l\'image');
                break;
            case 'bad-file':
                echo('Veuillez ajouter une image jpeg ou png');
                break;
            case 'large-file':
                echo('Veuillez ajouter une image moins lourde');
                break;
            case 'already-exist':
                echo('Veuillez choisir un autre username. Celui la est déjà pris');
                break;
        }
    }
    echo('</div>');
    ?>


    <input type="submit">
</form>
</body>
</html>
