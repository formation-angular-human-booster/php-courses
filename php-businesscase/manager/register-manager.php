<?php
// Ici je me connecte à ma BDD
require '../db-connexion.php';

// Ici je cré un tableau avec les extensions autorisées par mon application
// Dans ce cas je pourrais uploader des images jpeg ou des images png
$allowedExtension = [
    'image/jpeg',
    'image/png',

];

// J'ai traité tous les champs de mon formulaire
// Et je retirige mon utilisateur vers ma page de register pour afficher les erreurs
if(empty($_POST['username'])){
    header("Location: ../register.php?error=no-username");
}
elseif(empty($_POST['password'])){
    header("Location: ../register.php?error=no-password");
}

elseif(empty($_POST['confirm-password'])){
    header("Location: ../register.php?error=no-confirm");
}

elseif($_POST['password'] !== $_POST['confirm-password']){
    header("Location: ../register.php?error=not-same");
}

// Si j'arrive ici, c'est que je n'ai pas d'erreur au niveau de mon formulaire
// Grace à la superglobale file et son attribut error, je regard si la clé
// error du tableau n'est pas égal à 0, ça veut dire qu'il y a eu une erreur
// pendant l'upload
elseif($_FILES['profile_picture']['error'] != 0){
    header("Location: ../register.php?error=error-upload");
}

// Sinon je vérifie son type pour vérifier qu'un hacker n'injecte pas un
// executable. On vérifie que c'est bien une photo avec le tableau déclaré
// à la ligne 5
elseif(!in_array($_FILES['profile_picture']['type'], $allowedExtension)){
    header("Location: ../register.php?error=bad-file");
}

// Je continue les vérifs et je regarde que le fichier n'est pas trop lourd
// pour ne pas surcharger mon serveur.
elseif($_FILES['profile_picture']['size'] > 10000000){
    header("Location: ../register.php?error=large-file");
}

// Je génére un nom de fichier unique pour ne pas écraser les fichiers
// si 2 utilisateurs uploadent un fichier qui aurait le même nom
$fileName = uniqid().'.'.explode('/',$_FILES['profile_picture']['type'])[1];

// J'appel la fonction move_uploaded_file pour télécharger le fichier sur mon serveur
// Cette fonction prend 2 paramètres le chemin de provenance et le chemin de destination
// Dans la variables super globale file tmp_name on retrouve le chemin temporaire
// de stockage de notre fichier. On le place ensuite dans un dossier uploads que nous aurons
// créé précédement
move_uploaded_file($_FILES['profile_picture']['tmp_name'], '../images/'.$fileName );


// Ici je vais aller enregistrer mes éléments dans la BDD.
// Je commence par préparer ma requête

try {
    $req = $pdo->prepare("INSERT INTO utilisateur (username, password, image_link) 
VALUES (:username, :password, :image_link)");

// Je l'execute
    $req->execute([
        'username'=> $_POST['username'],
        'password'=> $_POST['password'],
        'image_link'=> $fileName
    ]);
    header('Location: ../login.php');
} catch (\PDOException $e){

    if($e->getCode() == '23000'){
        header('Location: ../register.php?error=already-exist');
        die();
    } else {
        var_dump($e->getCode());
        die();
    }
}
?>
