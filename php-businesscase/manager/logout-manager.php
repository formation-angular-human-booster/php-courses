<?php
    // Je modifie ma config php pour être comme vous et devoir systématiquement
    // utiliser la fonction session_start avant l'utilisation de la session
    ini_set("session.auto_start", 0);

    // Je vais utiliser ma session donc je suis obligé en PHP d'appeler
    // la fonction session_start avant. Sinon ça ne marche pas (en fonction de la config)
    session_start();

    // Je détruit ma session pour me déconnecter
    session_destroy();

    // L'utilisateur est déconecté donc je le renvoie vers le login

    header('Location: ../login.php');
?>