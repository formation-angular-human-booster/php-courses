<?php
    ini_set("session.auto_start", 0);
    session_start();
    // CAS ou l'utilisateur n'a saisie ni mot de passe ni username
    if(empty($_POST['username']) && empty($_POST['password'])){
        // Je renvoie mon utilisateur vers la page de login puisqu'il n'a rien saisie
        // Dans le paramètre get error, je lui indique la raison de son erreur
        header("Location: ../login.php?error=no-saisie");
    } elseif (empty($_POST['username'])){
        header("Location: ../login.php?error=no-username");
    } elseif (empty($_POST['password'])){
        header("Location: ../login.php?error=no-password");
    } else {
        if($_POST['username'] == 'lodevie' && $_POST['password'] == 'lodevie'){
            $_SESSION['username'] = $_POST['username'];
            header("Location: ../dashboard.php");
        } else {
            header("Location: ../login.php?error=bad-credentials");
        }
    }

?>