<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<?php
require 'db-connexion.php';


$response = $pdo
    ->query("SELECT * FROM utilisateur;");

$resultat = $response->fetchAll();

?>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Username</th>
        <th scope="col">Photo</th>
        <th scope="col">Action</th>
    </tr>
    </thead>

    <tbody>
    <?php
        foreach ($resultat as $user){
            echo('<tr>
        <th scope="row">'.$user['id'].'</th>
        <td>'.$user['username'].'</td>
        <td><img class="max-100"
        src="images/'.$user['image_link'].'" />
        </td>
        <td>
            <a href="user-detail.php?id='.$user['id'].'">Voir en détail</a>
            <a href="manager/delete-user.php?id='.$user['id'].'">Supprimer</a>
            <a href="edit-user-form.php?id='.$user['id'].'">Editer</a>
        </td>
    </tr>');
        }
        $response->closeCursor();
    ?>
    </tbody>
</table>

</body>
</html>

