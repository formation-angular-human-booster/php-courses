<?php
/*Exo 1 )
Créer un nouveau fichier php garage.php
créer une variable de type string modèle*/

$age = 17;

// Ceci est un ternaire pour développeur aguéri mais jeune
$isMajeur = ($age>= 18) ? 'Je suis majeur':'Je suis mineur';

// Equivalent pour les vieux développeurs
if($age>=18){
    $isMajeur = 'Je suis majeur';
} else {
    $isMajeur = 'Je suis mineur';
}


$cars = [
    ['marque'=> 'Tesla', 'model'=> 'Model S', 'nbPassenger'=> 5, 'energy'=> 'electric'],
    ['marque'=> 'Renault', 'model'=> 'Clio', 'nbPassenger'=> 4, 'energy'=> 'essence'],
    ['marque'=> 'Renault', 'model'=> 'Trafic', 'nbPassenger'=> 9, 'energy'=> 'diesel']
];




$modele = 'Model S';


/*
créer une variable de type string marque*/

$marque = 'Tesla';


/*
créer une variable de type integer nombre de cheveaux*/

$nbHorse = 190;

/*
Créer une variable nombre de passager*/

$nbPassenger = 5;

// Initialisation d'une variable de type string qui contiendra la chaine de caractère à afficher

$displayString = '';

/*
Si mon modèle est égal à "Model S" et que la marque est égale à Tesla ecrivez : "Tu ne peux pas aller loin avec ça !"*/

if($modele == 'Model S' && $marque == 'Tesla'){
    $displayString = $displayString . 'Tu ne peux pas aller loin avec cette caisse ! <br>';
}


/*

Si mon modele est égal à "Zoe" et la marque est égal à renault :
- Indiquez vous ne pouvez pas aller jusqu'a St just*/

if($modele == 'Zoe' && $marque == 'Renault'){
    $displayString = $displayString .'Tu ne peux pas aller jusqu\'à St Just ! <br>';
}

// - Si le nombre de passager est supérieur à 2 ajouter à la chaine de caractère ca ne sera pas très comfortable.

if($nbPassenger > 2 ){
    $displayString = $displayString . ' Ca ne sera pas confortable ! <br>';
}
// Si le nombre de cheveaux est suppérieur à 150, ajouter à toutes les chaines de caractères (trajet rapide)

if($nbHorse > 150 ){
    $displayString = $displayString . ' Le trajet sera rapide ! <br>';
}

//M Faire une jolie page HTML avec le nom, le modele de la voiture dans le header*/
?>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<header>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container-fluid">
            <button
                class="navbar-toggler"
                type="button"
                data-mdb-toggle="collapse"
                data-mdb-target="#navbarExample01"
                aria-controls="navbarExample01"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarExample01">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" aria-current="page" href="#">Toutes les voitures</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" aria-current="page" href="cars.php">Exercice 2</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures essence</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures diesel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures electriques</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar -->

    <!-- Jumbotron -->
    <div class="p-5 text-center bg-light">
        <h1 class="mb-3">
            <?php
            echo($marque);
            ?>
        </h1>
        <h4 class="mb-3"><?php echo($modele); ?></h4>
        <div>
            <?php
                echo($displayString);
            ?>
        </div>
    </div>
    <!-- Jumbotron -->
</header>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
