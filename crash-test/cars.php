<?php
    $cars = [
        [
            'energy'=> 'electric',
            'model'=> 'Model S',
            'marque'=> 'Tesla',
            'nbPassenger'=> 5,
            'image'=> 'images/tesla.jpeg'
        ],
        [
            'energy'=> 'Essence',
            'model'=> 'Clio',
            'marque'=> 'Renault',
            'nbPassenger'=> 4,
            'image'=> 'images/clio.jpg'
        ],
        [
            'energy'=> 'Diesel',
            'model'=> '118D',
            'marque'=> 'BMW',
            'nbPassenger'=> 5,
            'image'=> 'images/bmw.jpg'
        ],

    ];

?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<header>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container-fluid">
            <button
                class="navbar-toggler"
                type="button"
                data-mdb-toggle="collapse"
                data-mdb-target="#navbarExample01"
                aria-controls="navbarExample01"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarExample01">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" aria-current="page" href="garage.php">Toutes les voitures</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link" aria-current="page" href="cars.php">Exercice 2</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures essence</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures diesel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Les voitures electriques</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar -->

    <!-- Jumbotron -->
    <div class="p-5 text-center bg-light">


    </div>
    <!-- Jumbotron -->
</header>

<div class="container-fluid">
    <h2>Nous sommes le <?php echo(date("d-m-Y")); ?></h2>

    <h2>Aurélien est né le <?php echo(date('d-m-Y', 733486810));?></h2>
    <div class="row">

    <?php
    foreach ($cars as $car) {
        $class = ($car['energy'] === 'electric') ? 'bg-success' : 'bg-primary';

        echo '        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="'.$car['image'].'" alt="Card image cap">
            <div class="card-body '.$class.'">
                <h5 class="card-title">'.$car['model'].'</h5>
                <p class="card-text"> Voiture de '. $car['nbPassenger'].' places roulant à l\''.$car['energy'].'</p>
        <a href="#" class="btn btn-primary">'.$car['energy'].'</a>
    </div>
</div>';
    }
    ?>


    </div>


</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</body>
</html>
