<?php

// Solution 1
function countLetter($chaine, $searchLetter) {
    $nbLetter = 0;
    for($i=0; $i<strlen($chaine); $i++){
        if($chaine[$i] === $searchLetter){
            $nbLetter += 1;
        }
    }

    return $nbLetter;
}

// Solution
function countLetterSimple($chaine, $lettre){
    return substr_count($chaine, $lettre);
}


echo('Il y a '. countLetterSimple( 'Bonjour Aurélien', 'j').' fois la lettre j');

?>