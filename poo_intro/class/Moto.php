<?php
    class Moto extends Vehicule {
        private $hasBulle;

        public function __construct($nbRoues, $hasBulle)
        {
            $this->hasBulle = $hasBulle;
            parent::__construct($nbRoues);
        }
    }
?>