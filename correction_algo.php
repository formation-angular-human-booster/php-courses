<html>
<head>

</head>
<body>
<h1>Exercice 1 !</h1>
<h2>Retourner un string !

    exemple : Si on écrit "Bonjour ! " dans une variable, notre application devra afficher  "! ruojnoB"
    Si on ecrit :

    Ici ! Ici ! C'est Montferrand
    L'application devra afficher :
    dnarreftnoM tse'C ! icI ! icI
</h2>


<h3>Correction</h3>

<?php

    function reverseString($stringARetourner){
        for($i = strlen($stringARetourner)-1; $i>=0; $i--){
            echo($stringARetourner[$i]);
        }
    }
    $textARetourner = 'Bonjour';
    $textARetourner2 = 'Ici ! Ici ! C\'est montferrand !';

    echo(strrev($textARetourner));
    echo('<br>');
    echo(strrev($textARetourner2));
    echo('<br>');

    echo(reverseString('Toto'));



?>

<h1>Exercice 2 !</h1>
<h2>
    Créer une fonction qui retourne le nombre de caractère d'une chaine de caractère
    Exemple : "Aurélien" retournera le nombre 8
    Proposer 2 alternatives
</h2>

<h3>Correction</h3>

<?php
 $text = 'Aurélien';

 echo(mb_strlen($text));



 var_dump(count(mb_str_split($text)));
?>

<h1>Exercice 3 !</h1>

<h2>
    On vous demande d'afficher pour chacun des noms de domaine (ce qui est après l'@) de donner le nombre de fois que cet élément est présent dans le tableau
    Avec mon tableau, ça donnera cela :
</h2>
<h3>Correction !</h3>
<?php
$adressesMail = [
    'aureliendelorme1@gmail.com',
    'smithcrank@gmail.com',
    '148547@supinfo.com',
    'aurelien.delorme@orange.fr',
    'test@yahoo.com',
    'bonjour@msn.com',
    'adelorme@humanbooster.com',
    "test@gmail.com",
    "toto@gmail.com"
];
$arrayDomaineUser = [];
foreach ($adressesMail as $mail){
    $arrayDomaineUser[] = explode('@', $mail )[1];
}

$resultatsCount = array_count_values($arrayDomaineUser);
$arr = [];
foreach ($resultatsCount as $key=>$value){
    $arr[] = ['pourcentage'=> ($value/count($adressesMail))*100, 'domaine'=> $key, 'nombre'=> $value];

}

var_dump($arr);

?>
</body>
</html>