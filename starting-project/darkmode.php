<?php
// J'inclus le fichier de fonctions darkmode_functions
// Il contient 2 fonctions une pour réccupérer la valeur du darkmode
// Une pour mettre à jour la valeur du darkmode
include 'functions/darkmode_functions.php';

// Je réccupére le paramètre get mode envoyé dans index.php
if(isset($_GET['mode'])){
    // J'appel la fonction qui modifie mon darkmode. Cette fonction créera le cookie
    setDarkMode($_GET['mode']);
} else {
    echo('Une erreur s\'est produite');
    die();
}

// J'ai fini le traitement, je redirige l'utilisateur vers index.php
header('Location: index.php');
