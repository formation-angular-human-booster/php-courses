<html>
<head>
    <title>Mon super business case</title>

    <?php
    // Ici sera inclu tous nos fichiers css on va se positionner dedans pour gérer le darkmode
    include "parts/global-css.php";
    include "functions/user-function.php";
    checkUser();
    ?>
</head>
<body <?php

$mode = getDarkmode();

if($mode === 'dark'){
    echo('class="dark"');
}
?>
>
<div class="container">
    <?php
    include "parts/header.php";
    ?>
    <h1>Les articles en vente !</h1>
    <div class="row">
        <?php
        require_once 'functions/product-function.php';
        $products = getAllProducts();

        foreach ($products as $product) {
            displayProduct($product);
        }
        ?>
    </div>

    <?php
    include "parts/footer.php";
    ?>
</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>