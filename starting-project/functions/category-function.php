<?php
    function getAllCategory($pdo){
        // Je cré une requête SQL qui selectionne tous mes enregistrements
        $request = $pdo->query("SELECT * FROM categories ORDER BY ordre_affichage ASC");

        // Je retourne le resultat de la requête
        $result =  $request->fetchAll();

        $request->closeCursor();

        return $result;
    }

    function getOneCategory($pdo, $id){
        $request = $pdo->prepare("SELECT * FROM categories WHERE id=:id");
        $request->execute(['id'=> $id]);

        $result = $request->fetch();

        $request->closeCursor();

        return $result;
    }

    function findCategoryByName($pdo, $nom){
        $request = $pdo->prepare("SELECT * FROM categories WHERE nom=:nom");
        $request->execute(['nom'=> $nom]);

        $result = $request->fetch();

        $request->closeCursor();

        return $result;
    }

function findCategoryByDisplayOrder($pdo, $ordreAffichage){
    $request = $pdo->prepare("SELECT * FROM categories WHERE ordre_affichage=:ordre_affichage");
    $request->execute(['ordre_affichage'=> $ordreAffichage]);

    $result = $request->fetch();

    $request->closeCursor();

    return $result;
}

function  addCategory($pdo, $nom, $orderDisplay){
        $request = $pdo->prepare("INSERT INTO categories (nom, ordre_affichage)
        VALUE (:nom, :ordre_affichage)");

        $request->execute([
           'nom'=> $nom,
           'ordre_affichage'=> $orderDisplay
        ]);
}

function deleteCategory($pdo, $id){
        $request = $pdo->prepare("DELETE FROM categories WHERE id = :id");
        $request->execute(['id'=> $id]);
}

function findOtherCategoriesWithSameName($pdo, $nom, $id){
    $request = $pdo->prepare("SELECT * FROM categories WHERE nom=:nom AND id != :id");
    $request->execute(['nom'=> $nom, 'id'=> $id]);

    $result = $request->fetch();

    $request->closeCursor();

    return $result;
}

function findOtherCategoriesWithSameOrder($pdo, $ordre, $id){
    $request = $pdo->prepare("SELECT * FROM categories WHERE ordre_affichage=:ordre AND id != :id");
    $request->execute(['ordre'=> $ordre, 'id'=> $id]);

    $result = $request->fetch();

    $request->closeCursor();

    return $result;
}

function updateCategory($pdo, $id, $nom, $ordre){
        $request = $pdo->prepare("UPDATE categories SET nom = :nom, ordre_affichage=:ordre WHERE id = :id");
        $request->execute(["nom"=> $nom, 'ordre'=> $ordre, 'id'=> $id]);
    }


?>