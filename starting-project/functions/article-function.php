<?php



    function getArticleByCategory($pdo, $idCateg){
        $query = $pdo->prepare("SELECT * FROM article WHERE id_categories = :idCat");
        $query->execute(['idCat'=> $idCateg]);
        $result = $query->fetchAll();
        $query->closeCursor();
        return $result;
    }