<html>
<head>
    <title>Mon super business case</title>

    <link rel="stylesheet" href="public/css/login.css">
    <?php
    var_dump(password_hash("ddechamps", PASSWORD_DEFAULT));
    include "parts/global-css.php";
    ?>
</head>
<body>
<?php
include "functions/db-connect.php";
include "functions/user-function.php";

// Je cré une nouvelle variable qui contient un tableau vide
$errors = [];

// Je regarde la mèthode envoyée dans la requête HTTP
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // Ici je suis dans une requête de type POST ce qui signifie qu'un formulaire à été transmis
    if(!empty($_POST['email']) || !empty($_POST['password'])){
        // Je vérifi les données envoyées dans mon formulaire
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            // Ici le cas ou l'email n'est pas valide, j'ajoute l'erreur dans le tableau créé ligne 14
            $errors[] =
                "L'adresse email ".$_POST['email']." est considérée comme invalide.";
        }

        if(empty($_POST['email'])){
            $errors[] = 'Veuillez remplir le champ email';
        }

        if(empty($_POST['password'])){
            $errors[] = 'Veuillez saisir le password';
        }

        // On a pas d'erreur sur notre formulaire tout est OK on redirige l'utilisateur
        if(count($errors) == 0)
        {

            // Je réccupére mon utilisateur en fonction de son email sans vérifier le password
            $user = getUserByEmail($pdo, $_POST['email']);

            if(!$user){
                $errors[] = 'Aucun compte avec ce mail';
            } else {
                if(!password_verify($_POST['password'], $user['mot_de_passe'])){
                    $errors[] = 'Email ou mot de passe incorrecte';
                } else {
                    connectUser($user);
                    header('Location: index.php');
                }
            }
        }
    }
}

?>
<div class="container login-container">

    <div class="row">
        <div class="col-md-6 login-form-1">
            <!-- TODO : Ajoutez une jolie image ! -->
        </div>
        <div class="col-md-6 login-form-2">
            <h3>Login for Form 2</h3>
            <!--            Formulaire envoyé avec la mèthode POST sur cette même page      -->
            <form method="post" action="login.php">
                <div class="form-group">
                    <!-- le champ name de nos inputs seront le body de notre requête HTTP.
                     Nous les retrouverons grâce à la mèthode super globale POST-->
                    <input name="email" required type="text" class="form-control" placeholder="Your Email *" value="<?php
                    if(array_key_exists('email', $_POST)){
                        echo($_POST['email']);
                    }
                    ?>" />
                </div>
                <div class="form-group">
                    <input name="password" required type="password" class="form-control" placeholder="Your Password *" value="<?php
                    if(array_key_exists('password', $_POST)){
                        echo($_POST['password']);
                    } ?>"/>
                </div>
                <div class="form-group">
                    <input type="submit" class="btnSubmit" value="Login" />
                </div>
                <div class="form-group">
                    <a class="text-white" href="register.php">M'enregistrer</a>
                </div>

                <?php
                // Nous vérifions si il y a des erreurs de saisie utilisateur pour les afficher
                if(count($errors) > 0){
                    // CAS OU NOUS AVONS DES ERREURS
                    ?>
                    <h2>Les erreurs !</h2>
                    <ul>
                        <?php
                        // Afficher nos erreurs une par une
                        foreach ($errors as $error) {
                            echo('<li class="text-danger">'.$error.'</li>')  ;
                        }
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </form>
            <?php if(count($errors)>0){
                ?>
                <h3 class="text-danger">Les erreurs du formulaire</h3>

                <ol>
                    <?php
                    foreach ($errors as $error){
                        echo('<li>'.$error.'</li>');
                    }
                    ?>
                </ol>

                <?php
            }
            ?>
        </div>
    </div>
</div>

<?php
include "parts/global-scripts.php";
?>
</body>
</html>