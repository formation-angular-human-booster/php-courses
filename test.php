<html>
<head>

</head>
<body>

<!-- Ceci est un commentaire HTML -->


<h3>Ici j'affiche dans un H1 un titre en PHP</h3>
<h3>Ensuite, j'affiche la chaine de caractère "la date d'ajourd'hui, il a dit "hello""</h3>
<?php


$firstName = 'Aurélien';
$lastName = 'Delorme';

echo('<h1>Titre dynamique écrit en PHP</h1>');
echo('La date d\'aujourd\'hui il a dit "hello"');
?>
<br>

<h3>Ici, je cré 2 variables, une firstName et une lastName.
    J'affiche ensuite Bonjour concaténé avec la variable firstname</h3>

<?php
/*
 * phpinfo();
*/


echo('<br>');
echo('Bonjour ' . $firstName . ' ' . $lastName);

// Revient au même
echo('Bonjour ');
echo($firstName);
echo(' ' . $lastName)
?>

<h3>Les différents types de variables : </h3>

<?php
$variableString = 'Hello world';


$variableNumber = 42;
var_dump($variableNumber);

$variableBoolean = true;
var_dump($variableBoolean);

$size = 1.78;
var_dump($size);


?>

<h3>Ici je test une addition</h3>

<?php
$nombre1 = 1;
$nombre2 = 2;

echo('Résultat de l\'addition: ' . ($nombre1 + $nombre2));
var_dump($nombre1 + $nombre2);

$nombre1 = "1.5";
$nombre2 = "1.2";

var_dump($nombre2 + $nombre1);

?>

<h3>Tests des ifs ! </h3>
<?php
$result = $nombre1 + $nombre2;
if ($result === 2.7) {
    echo('Calcul valide !!');
} else {
    echo('Calcul invalide !');
}

$age = 18;
const AGE = 10;
$test = null;
if ($age < 18) {
    $test = true;
    echo(AGE);
    echo "Tu es mineur";
} elseif ($age < 65) {
    echo('||Hello');
} else {
    echo('Default case');
}

echo($test);
?>

<h3>Test d'une variable de type boolean : </h3>

<?php
if (isset($isAdmin) == false) {
    echo('La variable isAdmin n\'existe pas');
}

// Création d'un tableau
$listeCourse = ['Pomme', 'Poire', 'Nutella'];

// equivalent
$listeCourse2 = array('Pomme', 'Poire', 'Nutella');

var_dump($listeCourse);
var_dump($listeCourse2);

// Création d'un tableau vide
$array = [];

var_dump($array);

// lien : https://www.php.net/manual/en/function.array-push
$stack = array("orange", "banana");
$result = array_push($stack, "apple", "raspberry");
var_dump($stack);

// Equivalent :

$stack[] = "masque";
$stack[] = 'test';
var_dump($stack);


$identity = [
    'nom' => 'Delorme',
    'prenom' => 'Aurelien',
    'age' => '27'
];

var_dump($identity);

var_dump($identity['age']);


$identities = [
    [
        'nom' => 'Delorme',
        'prenom' => 'Aurelien',
        'age' => '27'
    ],
    [
        'nom'=> "Colin",
        "prenom"=> "Morgan",
        "age"=> "?"
    ]
];

var_dump($identities);
?>

<h3>Affichage de la liste de course</h3>

Avec un for
<ul>
<?php
for($i = 0; $i<count($stack); $i++){
    echo("<li>".$stack[$i]."</li>");
}
echo('</ul><br>Avec un foreach</br>
<ul>');
foreach ($stack as $elem){
    echo("<li>".$elem."</li>");
}
echo('</ul>');

for($j = 0; $j<1000; $j++){
    echo($j);
}


foreach ($identity as $key=>$value){
    echo('Une entrée dans mon tableau nom : ['.$key.'] valeur : '.$value.'<br>');
}


?>
</ul>


<h2>Sous titre statique</h2>
</body>
</html>
