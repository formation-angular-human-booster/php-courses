<?php
require_once 'pdo_connect.php';
require_once 'functions.php';
$errors = [];
$imageUrl = null;
if ( $_SERVER['REQUEST_METHOD'] === 'POST'){
    $returnValidation = validateForm();
    $errors = $returnValidation['errors'];

    if( count($errors) === 0) {
        addBdd($pdo, $returnValidation['image']);
        header('Location: list-planets.php');
    }
}
?>
<html>
<head>
    <head>
        <?php
        require_once 'stylesheets.php';
        ?>
    </head>
</head>
<body>

<?php
include 'nav.php';
?>

<div style="text-align: center">

    <h1>Ajouter une planète</h1><br>

    <form method="post" action="add-planet.php" enctype="multipart/form-data">
        <label>Nom de la planète</label>
        <input name="name" class="form-control" placeholder="Nom de la planète">
        <label>Status de la planète</label>
        <input name="status" class="form-control" placeholder="Status" >
        <label>Terrain</label>
        <input name="terrain" class="form-control" placeholder="Terrain" >
        <label>Allegiance</label>
        <select name="allegiance" class="form-control" placeholder="Allegiance" >
            <?php
                foreach (getAllegiances() as $allegiance) {
                    echo('<option value="'.$allegiance.'">'.$allegiance.'</option>');
                }
            ?>
        </select>
        <label>Key facts</label>
        <textarea name="key_fact" class="form-control" placeholder="Key facts" ></textarea><br>
        <label>Image</label>
        <input type="file" name="image"><br><br>

        <input type="submit">

        <?php
            if(count($errors) != 0){
                echo(' <h2>Erreurs lors de la dernière soumission du formulaire : </h2>');
                foreach ($errors as $error){
                    echo('<div class="error">'.$error.'</div>');
                }
            }
        ?>
    </form>
</div>
</body>
</html>
