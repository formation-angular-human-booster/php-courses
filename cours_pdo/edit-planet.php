<?php
require_once 'pdo_connect.php';
require_once 'functions.php';
$idPlanet = $_GET['id'];
$planet = getPlanet($pdo, $idPlanet);
$errors = [];
$imageUrl = null;
if ( $_SERVER['REQUEST_METHOD'] === 'POST'){
    $returnValidation = validateEditForm();
    $errors = $returnValidation['errors'];
    $imageUrl = $returnValidation['image'];

    if( count($errors) === 0) {
        updateBdd($pdo, $imageUrl, $planet['id']);
        header('Location: list-planets.php');
    }
}

?>
<html>
<head>
    <head>
        <?php
        require_once 'stylesheets.php';
        ?>
    </head>
</head>
<body>

<?php
include 'nav.php';
?>

<div style="text-align: center">

    <h1>Editer la planète todo</h1><br>

    <form method="post" action="edit-planet.php?id=<?php echo($planet['id']);?>" enctype="multipart/form-data">
        <label>Nom de la planète</label>
        <input name="name" value="<?php echo($planet['name']) ?>" class="form-control" placeholder="Nom de la planète">
        <label>Status de la planète</label>
        <input name="status" value="<?php echo($planet['status']) ?>" class="form-control" placeholder="Status" >
        <label>Terrain</label>
        <input name="terrain" value="<?php echo($planet['terrain']) ?>" class="form-control" placeholder="Terrain" >
        <label>Allegiance</label>
        <select name="allegiance" class="form-control" placeholder="Allegiance" >
            <?php

            foreach (getAllegiances() as $allegiance) {
                $selected = '';
                if($planet['allegiance'] === $allegiance){
                    $selected = 'selected';
                }
                echo('<option '.$selected.' value="'.$allegiance.'">'.$allegiance.'</option>');
            }
            ?>
        </select>
        <label>Key facts</label>
        <textarea name="key_fact" class="form-control" placeholder="Key facts" ><?php echo($planet['key_fact']) ?>
        </textarea><br>
        <label>Image :</label> <br>
        <img src="<?php echo('images/planets/'.$planet['image']);?>"><br><br>
        <input type="file" name="image" value="<?php echo($planet['image']) ?>"><br><br>

        <input type="submit">
    </form>
        <?php
        if(count($errors) != 0){
            echo(' <h2>Erreurs lors de la dernière soumission du formulaire : </h2>');
            foreach ($errors as $error){
                echo('<div class="error">'.$error.'</div>');
            }
        }
        ?>

</div>
</body>
</html>
