<?php
require_once 'pdo_connect.php';
?>
<html>
<head>
    <head>
        <?php
        require_once 'stylesheets.php';
        ?>
    </head>
</head>
<body>
<?php
include 'nav.php';
?>
<h1>Page qui liste nos planètes</h1>
<h2>
    <h1>Les planètes disponibles dans notre base de donnée :</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Status</th>
            <th scope="col">Terrain</th>
            <th scope="col">Allegiance</th>
            <th scope="col">Key facts</th>
            <th scope="col">Image</th>
            <th scope="col">Action</th>
            <!--<th scope="col">Action</th>-->
        </tr>
        </thead>
        <tbody>
        <?php
        $reponse = $pdo->query('SELECT * FROM planets');
        while ($data = $reponse->fetch())
        {
            ?>
            <tr>
                <td><?php echo($data['id']); ?></td>
                <td><?php echo($data['name']); ?></td>
                <td><?php echo($data['status']); ?></td>
                <td><?php echo($data['terrain']); ?></td>
                <td><?php echo($data['allegiance']); ?></td>
                <td><?php echo($data['key_fact']); ?></td>
                <td>
                    <img style="max-width: 140px;" src="<?php echo('images/planets/'.$data['image']); ?>"
                         alt="Image de la planète <?php echo($data['name']); ?>"/>
                </td>
                <td>
                    <a title="Voir le détail" href="planet-detail.php?id=<?php echo($data['id']); ?>">
                        <i class="fa fa-eye"></i>
                    </a>

                    <a title="Editer" href="edit-planet.php?id=<?php echo($data['id']); ?>">
                        <i class="fa fa-edit"></i>
                    </a>

                    <a title="Supprimer" href="delete-planet.php?id=<?php echo($data['id']);?>">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>


            </tr>
            <?php
        }
        $reponse->closeCursor();
        ?>

        </tbody>
    </table>
</h2>
</body>
</html>