<?php
var_dump($_POST);
var_dump($_FILES);

$errors = [];
$allowedExtension = ['image/jpeg', 'image/png', 'image/gif'];

if($_FILES['file_upload']['error'] == 0){
    if($_FILES['file_upload']['size']>1000000){
        $errors[] = 'Impossible ! Fichier trop lourd';
    }

    if(!in_array($_FILES['file_upload']['type'], $allowedExtension)){
        $errors[] = 'Impossible d\'uploader ce fichier nous voulons une image !';
    }
} else {
    $errors[] = 'Une erreur inconnue s\'est produite';
}

var_dump($errors);

if(count($errors) == 0){
    $extension = explode('/',$_FILES['file_upload']['type'])[1];
    $fileName = uniqid().'.'.$extension;
    move_uploaded_file($_FILES['file_upload']['tmp_name'], 'upload/'.$fileName);
    setcookie('profilePicture', $fileName, time() + 3600);

    header("Location: index.php");
}
?>