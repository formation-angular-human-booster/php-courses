<?php
    class SessionManager {

        private $attributes;

        public $membrePublique;

        // Sert à construire un objet. Il sera appelé à chaque fois que l'on utilisera new SessionManager
        public function __construct()
        {
            $this->membrePublique = 'Ouiiiii';
            echo('je passe dans mon constructeur </br>');
            $this->attributes = [];
        }

        // Méthode appelée quand je cherche à affecter une valeur à
        // un membre qui n'existe pas ou qui est privé
        public function __set($name, $value)
        {
           $this->attributes[$name] = $value;
        }

        public function __get($name) {
            return $this->attributes[$name];
        }
    }
?>