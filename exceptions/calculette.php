<?php
    class Calculette {
        public $nombre1;
        public $nombre2;

        public function __construct()
        {
        }

        public function additionner(){
            if ( is_null( $this->nombre1 ) ){
                throw new Exception('Veuillez saisir le nombre 1');
            }

            if ( is_null( $this->nombre2 ) ){
                throw new Exception('Veuillez saisir le nombre 2');
            }

            return $this->nombre1 + $this->nombre2;
        }
    }
?>