<?php
    class MaxCentException extends Exception {

        public function __construct($message = "", $code = 0, Throwable $previous = null)
        {
            parent::__construct($message, $code, $previous);
        }

        public function maxCent() {
            throw new Exception('Veuillez saisir un nombre');
        }
    }