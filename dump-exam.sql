-- MySQL dump 10.13  Distrib 8.0.24, for macos11 (x86_64)
--
-- Host: 127.0.0.1    Database: ecommerce-bc
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `roles` json NOT NULL,
                         `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `UNIQ_880E0D76F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'LaTerreEstPlate','[\"ROLE_ADMIN\"]','$argon2id$v=19$m=65536,t=4,p=1$GG88za/WZ7lQn4UZk3JZMg$qLTIAevnu2cvP6pv79EivG5MzMy7/+DiEXl78WRjMW4');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Sheba'),(2,'SpaceX'),(3,'Julius K9'),(4,'Purina');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorie` (
                             `id` int NOT NULL AUTO_INCREMENT,
                             `parent_id` int DEFAULT NULL,
                             `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UNIQ_497DD634989D9B62` (`slug`),
                             KEY `IDX_497DD634727ACA70` (`parent_id`),
                             CONSTRAINT `FK_497DD634727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `categorie` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,NULL,'Animaux','animaux'),(2,1,'Chiens','chiens'),(3,1,'Chats','chats'),(4,1,'Oiseaux','oiseaux'),(5,1,'Rongeurs','rongeurs'),(6,1,'Poissons','poissons'),(7,1,'Reptiles & Co','reptiles-co'),(8,1,'Animalerie 3.0','animalerie-3-0'),(9,NULL,'Nourriture','nourriture'),(10,NULL,'Hygiène','hygiene');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `client` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `roles` json NOT NULL,
                          `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                          `birth_date` date NOT NULL,
                          `full_adress` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                          `gender` tinyint(1) NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `UNIQ_C7440455E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'jon@doe.com','[\"ROLE_ADMIN\"]','$argon2id$v=19$m=65536,t=4,p=1$OKnmagxpqtnTwF/AYWh96Q$yD66zqyf5AQLVDUZAdtRtxcTSv+9WLsO6ND9Re+dO38','Jon','Snow','1997-01-05','Le mur',0),(2,'ilove@lebucher.com','[\"ROLE_USER\"]','$argon2id$v=19$m=65536,t=4,p=1$LEJ3tGw6OuaLaOdwkg6zBA$SuFWd8m5yHYrNAS1MFqYu7VuwnjWM0x9I9cOqvj5InQ','Jeanne','D\'arc','2000-12-10','Le bucher',1),(3,'jack@lepirate.com','[\"ROLE_ADMIN\"]','$argon2id$v=19$m=65536,t=4,p=1$dvkRrhZTT0vkdBYkeLrHiQ$D2+j4ROaYeopuDpZjUfGulxxXy1mBlWocwzCrO3tJ8M','Jack','Sparrow','1980-09-11','Le black pearl',0),(4,'test@test.test','[\"ROLE_USER\"]','$argon2id$v=19$m=65536,t=4,p=1$OmBjRTWsIWf1YPkkC6GVDQ$nxkt7XxiOsTVOue/VouZ6V2sJrQuvdJneoSyYgVUb6o','test','test','2000-12-10','test',1);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_form`
--

DROP TABLE IF EXISTS `contact_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact_form` (
                                `id` int NOT NULL AUTO_INCREMENT,
                                `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `phone` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_form`
--

LOCK TABLES `contact_form` WRITE;
/*!40000 ALTER TABLE `contact_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `state_id` int NOT NULL,
                         `client_id` int NOT NULL,
                         `total_price` double DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         KEY `IDX_F52993985D83CC1` (`state_id`),
                         KEY `IDX_F529939819EB6921` (`client_id`),
                         CONSTRAINT `FK_F529939819EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
                         CONSTRAINT `FK_F52993985D83CC1` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,2,1,123),(2,4,1,569),(3,2,3,276),(4,4,2,234);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photo` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `product_id` int NOT NULL,
                         `link` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                         PRIMARY KEY (`id`),
                         KEY `IDX_14B784184584665A` (`product_id`),
                         CONSTRAINT `FK_14B784184584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` VALUES (1,1,'http://place-hold.it/350x150'),(2,2,'http://place-hold.it/350x150'),(3,3,'http://place-hold.it/350x150'),(4,4,'http://place-hold.it/350x150');
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `categorie_id` int DEFAULT NULL,
                           `brand_id` int NOT NULL,
                           `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                           `excltax_price` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
                           `quantity` int DEFAULT NULL,
                           `is_active` tinyint(1) NOT NULL,
                           PRIMARY KEY (`id`),
                           KEY `IDX_D34A04ADBCF5E72D` (`categorie_id`),
                           KEY `IDX_D34A04AD44F5D008` (`brand_id`),
                           CONSTRAINT `FK_D34A04AD44F5D008` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
                           CONSTRAINT `FK_D34A04ADBCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,2,1,'Croquettes au poulet pour chien','Lorem ipsum','15.95',NULL,1),(2,3,4,'Paté au boeuf pour chats','Lorem ipsum','11.95',NULL,1),(3,8,2,'Voyage sur la lune!','Lorem ipsum','140000',NULL,1),(4,6,1,'Nourriture pour poisson','Lorem ipsum','9.95',NULL,1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_order`
--

DROP TABLE IF EXISTS `product_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_order` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `product_id` int NOT NULL,
                                 `order_id` int NOT NULL,
                                 `quantity` int DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `IDX_5475E8C44584665A` (`product_id`),
                                 KEY `IDX_5475E8C48D9F6D38` (`order_id`),
                                 CONSTRAINT `FK_5475E8C44584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
                                 CONSTRAINT `FK_5475E8C48D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_order`
--

LOCK TABLES `product_order` WRITE;
/*!40000 ALTER TABLE `product_order` DISABLE KEYS */;
INSERT INTO `product_order` VALUES (1,2,1,4),(2,1,4,3),(3,3,1,1),(4,1,2,2);
/*!40000 ALTER TABLE `product_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `state` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'En cours'),(2,'Accépté'),(3,'Remboursé'),(4,'Expédié');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-30  8:34:47
