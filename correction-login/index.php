<html>
<head>

</head>
<body>

<?php


    if(!empty($_SESSION) && $_SESSION['username']){
        header("Location: dashboard.php");
    }
?>
<h1>Login</h1>
<form method="post" action="login-manager.php">
    <label>Username</label>
    <input type="text" name="username" placeholder="Veuillez saisir votre nom d'utilisateur">

    <label>Password</label>
    <input type="password" name="password" placeholder="Mot de passe">

    <input type="submit">

    <?php
        if(!empty($_GET) && isset($_GET['error'])){
            switch ($_GET['error']){
                case 'bad-credentials':
                    echo('Les identifiants sont incorrecte');
                    break;
                case 'no-username'   :
                    echo('Veuillez saisir un username');
                    break;
                case 'no-password':
                    echo('Veuillez saisir un password');
                    break;
            }
        }
    ?>
</form>
</body>
</html>