<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
<?php
var_dump($_COOKIE);
$cars = [
    [
        'energy'=> 'electric',
        'model'=> 'Model S',
        'marque'=> 'Tesla',
        'nbPassenger'=> 5,
        'image'=> 'images/tesla.jpeg'
    ],
    [
        'energy'=> 'Essence',
        'model'=> 'Clio',
        'marque'=> 'Renault',
        'nbPassenger'=> 4,
        'image'=> 'images/clio.jpg'
    ],
    [
        'energy'=> 'Diesel',
        'model'=> '118D',
        'marque'=> 'BMW',
        'nbPassenger'=> 5,
        'image'=> 'images/bmw.jpg'
    ],

];
if(empty($_SESSION) || !isset($_SESSION['username'])){
    header("Location: index.php");
}

echo('Bonjour '. $_SESSION['username']);
?>
<div class="row">
<a href="logout.php">Me déconecter</a>
    <?php
        $nbElem = count(explode(',', $_COOKIE['panier']));
    ?>
    <a href="panier.php">Visualiser mon panier (<?php echo($nbElem)?> articles)</a>
</div>
  <div class="row">

    <?php
    foreach ($cars as $car) {
        $class = ($car['energy'] === 'electric') ? 'bg-success' : 'bg-primary';

        echo '        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="'.$car['image'].'" alt="Card image cap">
            <div class="card-body '.$class.'">
                <h5 class="card-title">'.$car['model'].'</h5>
                <p class="card-text"> Voiture de '. $car['nbPassenger'].' places roulant à l\''.$car['energy'].'</p>
        <a href="#" class="btn btn-primary">'.$car['energy'].'</a>
        
         <a href="add-favori.php?model='.$car['model'].'" class="btn btn-primary">Ajouter '.$car['model'].' au panier</a>
    </div>
</div>';
    }
?>
</body>
</html>
