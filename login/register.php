<html>
<head>

</head>

<body>
<?php
    require_once "database_connect.php";
    $fileName = null;
    $errors = [];

    if(!empty($_POST)){

    if (isset($_FILES['profil_picture']) && $_FILES['profil_picture']['error'] == 0){
        if($_FILES['profil_picture']['size'] >= 1000000000){
         $errors[] = "Fichier trop lourd !!";
        } else {
            $extension_upload = $_FILES['profil_picture']['type'];

            $extension = explode("/", $extension_upload)[1];


            $fileName = 'uploads/profile/'.uniqid().'.'.$extension;



            move_uploaded_file($_FILES['profil_picture']['tmp_name'], $fileName);
        }

    } else {
        $errors[] = 'Erreur lors de l\'upload de l\'image !!';
    }

        if(empty($_POST['username'])){
           $errors[] = 'Veuillez indiquer votre username !!';
       }

        if(empty($_POST['password'])){
            $errors[] = 'Veuillez indiquer votre password !!';
        }


        if(count($errors) == 0) {
            $request = $bdd->prepare('INSERT INTO user (username, pass, avatar) VALUES(:name, :pass, :avatar)');
            $request->execute([
                'name'=> $_POST['username'],
                'pass'=> $_POST['password'],
                'avatar'=> $fileName
            ]);
        }

    } else {
        echo("REQUEST GET CALL");
    }
?>
<h1>M'enregistrer</h1>
<form method="post" enctype="multipart/form-data">
    <label>Username</label>
    <input type="text" name="username">
    <label>Password</label>
    <input type="text" name="password">
    <input type="file" name="profil_picture">
    <input type="submit">
    <ul>
    <?php
       foreach ($errors as $error){
           echo '<li>'.$error."</li>";
       }
    ?>
    </ul>
</form>
</body>
</html>
