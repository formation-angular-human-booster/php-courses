<html>
<head>
    <?php
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Edition du véhicule <?php
        echo($vehicule->getModele().' '.$vehicule->getMarque());
        ?></h1>


    <?php
    include 'Vue/parts/menu.php';
    ?>

    <a href="index.php?controller=car&action=list">Retour</a>

    <!-- On précise juste la méthode post ce qui fait que l'utilisateur passera dans la même fonction de mon controlleur
    En revanche cette fois çi il passera dans mon if($_SERVER['REQUEST_METHOD'] == 'POST'
    Cette fois, le formulaire sera pré-remplis avec les valeurs de l'objet que je souhaite mettre à jour
    -->
    <form method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Marque</label>
            <input type="text"
                   required="required"
                   value="<?php echo($vehicule->getMarque()) ?>"
                   class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="marque" placeholder="Marque du véhicule">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir la marque</small>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Modèle</label>
            <input
                    required="required"
                value="<?php echo($vehicule->getModele()) ?>"
                type="text" class="form-control" id="modele" aria-describedby="modeleHelp" name="modele" placeholder="Modèle du véhicule">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir le modèle</small>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php
    require'Vue/parts/form-error.php';
    ?>
</div>
</body>

<?php
include 'Vue/parts/global-scripts.php';
?>

</html>