<html>
<head>
    <?php
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Ajout d'un véhicule</h1>


    <?php
    include 'Vue/parts/menu.php';
    ?>

    <a href="index.php?controller=car&action=list">Retour</a>

   <!-- On précise juste la méthode post ce qui fait que l'utilisateur passera dans la même fonction de mon controlleur
    En revanche cette fois çi il passera dans mon if($_SERVER['REQUEST_METHOD'] == 'POST'-->
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="exampleInputEmail1">Marque</label>
            <input type="text"
                   value="<?php echo((isset($lastSaisie['marque'])) ? $lastSaisie['marque'] : '') ?>"
                   required="required"
                   class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="marque" placeholder="Marque du véhicule">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir la marque</small>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Modèle</label>
            <input
                    value="<?php echo((isset($lastSaisie['modele'])) ? $lastSaisie['modele'] : '') ?>"
                    type="text"      required="required" class="form-control" id="modele" aria-describedby="modeleHelp" name="modele" placeholder="Modèle du véhicule">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir le modèle</small>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Modèle</label>
            <input type="file"       class="form-control"  aria-describedby="modeleHelp" name="picture">
            <small id="emailHelp" class="form-text text-muted">Veuillez saisir le modèle</small>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php
        // Ici on afficher toutes les erreurs de notre formulaire.
        // Le template d'affichage est situé dans Vue/pars/form-error.php
        require'Vue/parts/form-error.php';
    ?>
</div>
</body>

<?php
include 'Vue/parts/global-scripts.php';
?>

</html>