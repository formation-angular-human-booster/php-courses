<html>
<head>
    <?php
    // Inclusion du CSS global de l'application
    include 'Vue/parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Affichage de tous les véhicules</h1>

    <?php
    // Inclusion de mon menu
        include 'Vue/parts/menu.php';
    ?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Marque</th>
            <th scope="col">Modèle</th>
            <th scope="col">Photo</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // Je parcours mon tableau de véhicule
        foreach ($vehicules as $vehicule){
            // Pour chacun des véhicules,
            // j'appelle les getteurs (fonction publique qui retourne des attributs privés)
            // pour afficher la valeur des attributs privés

            echo('<tr>
            <th scope="row">'.$vehicule->getId().'</th>
            <td>'.$vehicule->getMarque().'</td>
            <td>'.$vehicule->getModele().'</td>
            ');

            if($vehicule->getImage()){
                echo('<td><img src="'.$vehicule->getImage().'"/></td>');
            } else {
                echo('<td>No image ! </td>');
            }

            echo('<td>
            <!-- Génération d\'un lien vers la page de détail d\'un vehicule -->
               <a href="index.php?controller=car&action=detail&id='.$vehicule->getId().'">
                Voir en détail 
            </a>
             <!-- Génération d\'un lien vers la page d\'édition d\'un vehicule -->
              <a href="index.php?controller=car&action=edit&id='.$vehicule->getId().'">
                Editer
            </a>
            
               <!-- Génération d\'un lien vers la page d\'édition d\'un vehicule -->
                <a href="index.php?controller=car&action=delete&id='.$vehicule->getId().'">
            Supprimer
                </a>
            </td>
        </tr>');
        }
        ?>

        </tbody>
    </table>
</div>
</body>

<?php
// Inclusion de tous les scripts communs à toute l'application
include 'Vue/parts/global-scripts.php';
?>

</html>