<?php

class UtilisateurManager extends DbConnexion implements UtilisateurManagerInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($username, $password)
    {
        // De base mon utilisateur est nulle
        $utilisateur = null;

        // Je retrouve un utilisateur en fonction de son username
        $user = $this->findByUsername($username);

        if($user){
            // Il a trouvé un utilisateur il vérifie si le hash correspond
            if(password_verify($password,  $user->getPassword())){
                $utilisateur = $user;
            }
        }
        return $utilisateur;
    }


    // Selectionne un utilisateur en fonction de son username
    public function findByUsername($username) {
        $utilisateur = null;
        // Requete préparée
        $query = $this->bdd->prepare("SELECT * FROM utilisateur WHERE username = :username");
        $query->execute(['username'=> $username]);
        $utilisateurFrom    // Selectionne un utilisateur en fonction de son username
    public function findByUsername($username) {
            $utilisateur = null;
            // Requete préparée
            $query = $this->bdd->prepare("SELECT * FROM utilisateur WHERE username = :username");
            $query->execute(['username'=> $username]);
            $utilisateurFromBdd = $query->fetch();

            // Transforme notre tableau en objet si l'on a un utilisateur avec ce username en BDD
            if($utilisateurFromBdd){
                $utilisateur = new Utilisateur(
                    $utilisateurFromBdd['nom'], $utilisateurFromBdd['prenom'],
                    $utilisateurFromBdd['email'], $utilisateurFromBdd['username'],
                    $utilisateurFromBdd['password'],
                    $utilisateurFromBdd['id']);
            }

            // Retourne null si pas d'utilisateur avec cet ID sinon retourne l'utilisateur
            return $utilisateur;
        }Bdd = $query->fetch();

        // Transforme notre tableau en objet si l'on a un utilisateur avec ce username en BDD
        if($utilisateurFromBdd){
            $utilisateur = new Utilisateur(
                $utilisateurFromBdd['nom'], $utilisateurFromBdd['prenom'],
                $utilisateurFromBdd['email'], $utilisateurFromBdd['username'],
                $utilisateurFromBdd['password'],
                $utilisateurFromBdd['id']);
        }

        // Retourne null si pas d'utilisateur avec cet ID sinon retourne l'utilisateur
        return $utilisateur;
    }

    public function findByEmail($email) {
        $utilisateur = null;
        $query = $this->bdd->prepare("SELECT * FROM utilisateur WHERE email = :email");
        $query->execute(['email'=> $email]);
        $utilisateurFromBdd = $query->fetch();

        if($utilisateurFromBdd) {
            $utilisateur = new Utilisateur(
                $utilisateurFromBdd['nom'], $utilisateurFromBdd['prenom'],
                $utilisateurFromBdd['email'], $utilisateurFromBdd['username'],
                $utilisateurFromBdd['password'],
                $utilisateurFromBdd['id']);
        }

        return $utilisateur;
    }

    // Fonction retourne true si un utilisateur à déjà cet email
    // Sinon elle retourne false
    public function testExistUtilisateurByEmail($email){
        $user = $this->findByEmail($email);

        if($user){
            return true;
        } else {
            return false;
        }
    }

    // Fonction retourne true si un utilisateur à déjà ce username
    // Sinon elle retourne false
    public function testExistUtilisateurByUsername($username){
        $user = $this->findByUsername($username);
        if($user){
            return true;
        } else {
            return  false;
        }
    }

    public function register(Utilisateur $utilisateur){

        $utilisateur->setPassword(password_hash($utilisateur->getPassword(), PASSWORD_DEFAULT));

        $query = $this->bdd->prepare("INSERT INTO utilisateur (nom, email, prenom, username, password)
        VALUES (:nom, :email, :prenom, :username, :password )");

        $res = $query->execute([
            'nom'=> $utilisateur->getNom(),
            'email'=> $utilisateur->getEmail(),
            'prenom'=> $utilisateur->getPrenom(),
            'username'=> $utilisateur->getUsername(),
            'password'=> $utilisateur->getPassword()
        ]);
    }
}