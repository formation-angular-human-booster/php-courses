<?php
// Nos classes représentent les champs de notre BDD sous forme d'objet.
// Nous aurons en général 1 attribut pour chaque colonne
    class Vehicule{
        private $id;
        private $marque;
        private $modele;


        public function __construct($marque, $modele, $id=null){
            $this->id = $id;
            $this->marque = $marque;
            $this->modele = $modele;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function setMarque($marque){
            $this->marque = $marque;
        }

        public function getMarque(){
            return $this->marque;
        }


        /**
         * @return mixed
         */
        public function getModele()
        {
            return $this->modele;
        }

        /**
         * @param mixed $modele
         */
        public function setModele($modele): void
        {
            $this->modele = $modele;
        }
    }
?>